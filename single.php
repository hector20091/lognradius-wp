<?php
/**
 * The template for displaying all single post types
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header();

while ( have_posts() )
{
    the_post();

    get_template_part( 'templates/' . get_post_type(), 'single' );
}

get_footer();
