<?php
/**
 * Template part for displaying section with graphic-image card item
 */

$card_title    = get_sub_field('title');        // string
$card_text     = get_sub_field('text');         // string
$card_image    = get_sub_field('image');        // array( 'url', 'alt', 'title' )
$card_image_bg = get_sub_field('bg_color');     // hex color
$card_button   = get_sub_field('button_after'); // array( 'url', 'text' )
?>

<div>
    <div class="image"
         style="
             background-image: url('<?= $card_image['url'] ?>');
             background-color: <?= $card_image_bg ?>;
             height: 400px;
         ">
        <p class="type"></p>
    </div>

    <div class="heading">
        <h4><?= $card_title ?></h4>
    </div>

    <div class="text">
        <p><?= $card_text ?></p>
    </div>

    <div class="actions">
        <?php if ( $card_button ) : ?>

            <a href="<?= $card_button['url'] ?>" title="<?= $card_title ?>">
                <?= $card_button['text'] ?>
            </a>

        <?php endif; ?>
    </div>
</div>
