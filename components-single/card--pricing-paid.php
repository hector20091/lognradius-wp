<div class="bg-light-01">
    <h3>Developer</h3>

    <div class="price-numbers-block">

        <h4 class="price-number-block price-block2 price-number-block-dev tooltip-container"><span class="price-number">$<span id="priceD">7</span></span>/month<span class="tooltip">If you are a young startup you might be eligible for our Startup Discount. Contact us for more information.</span></h4>

        <p class="mau tooltip-container"><b><span id="mauD">500</span></b> Monthly active users <span class="tooltip">Monthly Active User (MAU) is a user who signs up, authenticates and/or uses an authentication token on the platform at least once in a calendar month. Once authenticated, the user can login unlimited number of times during that calendar month.</span></p>

    </div>

    <ul>
        <li class="tooltip-container"><b><span>Free Plans +</span></b><span class="tooltip">All features from the Free plans and more.</span></li>



        <li class="tooltip-container"><span>3 Web or/and mobile Apps</span><span class="tooltip">Have LoginRadius serve as the the authentication platform for multiple websites and mobiles application.</span></li>

        <li class="tooltip-container"><span>Registration form</span><span class="tooltip">Create up to 10 Standard fields for your registration forms</span></li>

        <li class="tooltip-container"><span>Web and mobile SSO</span><span class="tooltip">Seamless authentication among multiple applications for your consumers.</span></li>

        <li class="tooltip-container"><span>Basic user management</span><span class="tooltip">Perform basic Customer Profile actions such as edit, block,profile creation and password resets directly from the LoginRadius Dashboard.</span></li>

        <li class="tooltip-container"><span>Regional data storage</span><span class="tooltip">Host your users data at the preferred geographic location with a cloud based, scalable, secure and high-performant data repository</span></li>

        <li class="tooltip-container"><span>Email support</span><span class="tooltip">Email support from LoginRadius engineers during business hours.</span></li>

        <li><a href="#comparison">See all features</a></li>

    </ul>

    <div class="bottom card-developer replace-with-contact-sales-dev">
        <a href="" class="btn-primary">START FREE TRIAL</a>

        <p class="description-supporting">
            <b>Free 21-Day Trial.</b><br>
            No credit card required.<br>
            No hidden fees.
        </p>
    </div>
</div>
