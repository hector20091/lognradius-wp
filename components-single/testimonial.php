<?php
/**
 * Template part for displaying testimonial item
 */

$testimonial = [
    'name'     => get_field('name'),
    'photo'    => get_the_post_thumbnail_url(),
    'position' => get_field('position'),
    'company'  => get_field('company'),
    'logo'     => get_field('logo'),
    'link'     => get_field('link'),
    'text'     => get_the_content(),
]
?>

<div class="grid-25-75">
    <div class="graphic">
        <div class="person">
            <img alt="Elizabeth Melito" src="<?= $testimonial['photo'] ?>">
        </div>

        <div class="logo">
            <img alt="CBC Logo" class="small" src="<?= $testimonial['logo'] ?>">
        </div>
    </div>

    <div class="text">
        <div class="block-quote">
            <p><?= $testimonial['text'] ?></p>
        </div>

        <div>
            <p>
                <b><?= $testimonial['name'] ?></b><br>
                <?= $testimonial['position'] ?><br>

                <?php if( $testimonial['link'] ) : ?>

                    <a href="<?= $testimonial['link']['url'] ?>">
                        <?= $testimonial['company'] ?>
                    </a>

                <?php else :
                    echo $testimonial['company'];
                endif;
                ?>
            </p>
        </div>
    </div>
</div>
