<div class="bg-light-01">
    <h3>Authentication</h3>

    <p class="description">Quickly add LoginRadius auth to your web and mobile apps.</p>

    <ul>
        <li class="tooltip-container"><span>1 web or mobile app</span><span class="tooltip">Have LoginRadius serve as the the authentication platform for your website and mobile application</span></li>

        <li class="tooltip-container"><span>Standard login</span><span class="tooltip">User registration and authentication using unique ID (email address or username) and password.</span></li>

        <li class="tooltip-container"><span>Registration form</span><span class="tooltip">Collect up to 6 standard fields as part of your Registration process.</span></li>

        <li class="tooltip-container"><span>7,000 monthly active users</span><span class="tooltip">This is the limit for our free forever plan, upgrade to our paid plans to increase your limit</span></li>
    </ul>

    <div class="bottom">
        <a href="" class="btn-primary">FREE SIGN UP</a>

        <p class="description-supporting">
            <b>Free Forever.</b><br>
            No credit card required.<br>
            No hidden fees.
        </p>
    </div>
</div>
