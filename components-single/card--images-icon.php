<?php
/**
 * Template part for displaying section with graphic-image card item
 *
 * @param array $args
 */

$card = $args;
?>

<div class="icon-bg">
    <div class="image" style="background-image: url('/wp-content/themes/loginradius/images/cards/home/blogs--on-light.svg'); background-color: rgb(227, 241, 248);">
    </div>

    <div class="heading">
        <h4>Blogs</h4>
    </div>

    <div class="text" style="bottom: -81px;">
        <p>Infographics, interesting discussions, tips and tricks and much more you can find in our blogs.</p>
    </div>

    <div class="actions cardsShowcaseReveal">
        <a href="/blog">Visit</a>
    </div>
</div>
