<?php
/**
 * Template part for displaying integration item card
 */

$integration = [
	'title' => get_the_title(),
	'logo'  => get_the_post_thumbnail_url(),
	'link'  => get_permalink()
]
?>

<li class="integration-box">
    <a href="<?= $integration['link'] ?>" title="<?= $integration['title'] ?>">
        <div class="content">
            <img class="app-icon lazyloaded"
                 alt="<?= $integration['title'] ?>"
                 data-src="<?= $integration['logo'] ?>"
                 src="<?= $integration['logo'] ?>">
            <p class="integration-title"><?= $integration['title'] ?></p>
        </div>
    </a>
</li>
