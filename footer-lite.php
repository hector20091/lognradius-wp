<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package loginradius
 */

?>

<footer id="footer" class="site-footer">
    <div class="container">

        <div class="site-info">
            <?php the_custom_logo(); ?>
        </div>

    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
