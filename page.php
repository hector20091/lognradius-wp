<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();

while ( have_posts() ) {
	the_post();

	if ( have_rows( 'components_sections' ) ) {
		while ( have_rows( 'components_sections' ) ) {
			the_row();
			if ( get_row_layout() ) {
				get_template_part( 'components-sections/' . get_row_layout() );
			}
		}
	}
}

get_footer();
