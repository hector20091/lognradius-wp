<?php
/**
 * Register taxonomies
 */
function add_new_taxonomies()
{
	$taxonomies = [
		[
			'name'      =>  'Categories',
			'slug'      =>  'resource_cat',
			'post-type' =>  'resource'
		],

		[
			'name'      =>  'Categories',
			'slug'      =>  'integration_cat',
			'post-type' =>  'integration'
		]
	];

	foreach ( $taxonomies as $taxonomy )
	{
        $name      = $taxonomy['name'];
        $slug      = $taxonomy['slug'];
        $post_type = $taxonomy['post-type'];

        $labels = [
	        'name'                       => $name,
	        'singular_name'              => $name,
	        'search_items'               => 'Search '.$name,
	        'popular_items'              => 'Popular '.$name,
	        'all_items'                  => 'All '.$name,
	        'parent_item'                => null,
	        'parent_item_colon'          => null,
	        'edit_item'                  => 'Edit '.$name,
	        'update_item'                => 'Update '.$name,
	        'add_new_item'               => 'Add new '.$name,
	        'new_item_name'              => "New {$name} name",
	        'separate_items_with_commas' => "Separate {$name} with commas",
	        'add_or_remove_items'        => 'Add or remove '.$name,
	        'choose_from_most_used'      => 'Choose from most used '.$name,
	        'menu_name'                  => $name
        ];

        $args = [
            'hierarchical'          => true,
            'labels'                => $labels,
            'show_in_nav_menus'     => true,
            'show_ui'               => true,
            'show_in_rest'          => true,
            'show_tagcloud'         => true,
            'show_in_quick_edit'    => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => array(
                'slug'              => $slug,
                'hierarchical'      => true
            )
        ];

        register_taxonomy( $slug, $post_type, $args );
    }
}
add_action( 'init', 'add_new_taxonomies', 0 );


/**
 * Register post types.
 *
 * To create new post type add in the array
 */
function add_new_post_types()
{
	$post_types = [
		[
			'name'          => 'Resource',
			'menu_name'     => 'Resources',
			'slug'          => 'resource',
			'icon'          => 'dashicons-book',
			'hierarchical'  => true,
			'position'      => 21
		],

		[
			'name'          => 'Compliance',
			'menu_name'     => 'Compliance',
			'slug'          => 'compliance',
			'icon'          => 'dashicons-shield-alt',
			'hierarchical'  => true,
			'position'      => 22
		],

		[
			'name'          => 'Integration',
			'menu_name'     => 'Integrations',
			'slug'          => 'integration',
			'icon'          => 'dashicons-networking',
			'hierarchical'  => true,
			'position'      => 23
		],

		[
			'name'          => 'Testimonial',
			'menu_name'     => 'Testimonials',
			'slug'          => 'testimonial',
			'icon'          => 'dashicons-editor-quote',
			'hierarchical'  => true,
			'position'      => 25
		],
	];

    foreach( $post_types as $post )
    {
	    $name = $post['name'];
    	
	    $labels = [
		    'name'               => $name,
		    'singular_name'      => $name,
		    'add_new'            => "Add ".$name,
		    'add_new_item'       => 'Add new '.$name,
		    'edit_item'          => 'Edit '.$name,
		    'new_item'           => 'New '.$name,
		    'all_items'          => 'All '.$name,
		    'view_item'          => 'View '.$name,
		    'search_items'       => 'Search '.$name,
		    'not_found'          => $name.' are not found.',
		    'not_found_in_trash' => $name.' are not found in trash.',
		    'menu_name'          => $post['menu_name']
	    ];

        $args = [
            'labels'        => $labels,
            'public'        => true,
            'show_ui'       => true,
            'show_in_rest'  => true,
            'has_archive'   => true,
            'hierarchical'  => true,
            'menu_icon'     => $post['icon'],
            'menu_position' => $post['position'],
            'supports'      => [ "title", "editor", "thumbnail", "custom-fields", "revisions", "author" ],
        ];

        register_post_type( $post['slug'], $args );
    }
}
add_action( 'init', 'add_new_post_types', 0 );


/**
 * Create columns for post type resource
 */
function add_resource_column( $columns )
{
	$taxonomy = 'resource_cat';
	$name     = 'Categories';
	$new_columns[ $taxonomy ] = $name;
	return array_slice( $columns, 0, 2 ) + $new_columns + array_slice( $columns, 2 );
}

/* Add data to column in admin. */
function fill_resource_column( $colname, $post_id )
{
	$taxonomy = 'resource_cat';
	if ( $colname === $taxonomy )
	{
		$terms = wp_get_post_terms( $post_id, $taxonomy );
		foreach ( $terms as $term ) echo $term->name . '<br>';
	}
}

/* Make column sortable */
function add_resource_sortable_column( $sortable_columns )
{
	$taxonomy = 'resource_cat';
	$sortable_columns[ $taxonomy ] = [ $taxonomy, false ];
	return $sortable_columns;
}

add_filter( 'manage_' . 'resource' . '_posts_columns', 'add_resource_column', 4 );
add_action( 'manage_' . 'resource' . '_posts_custom_column', 'fill_resource_column', 5, 2 );
add_filter( 'manage_edit-' . 'resource' . '_sortable_columns', 'add_resource_sortable_column' );


/**
 * Create columns for post type integration
 */
function add_integration_column( $columns )
{
	$taxonomy = 'integration_cat';
	$name     = 'Categories';
	$new_columns[ $taxonomy ] = $name;
	return array_slice( $columns, 0, 2 ) + $new_columns + array_slice( $columns, 2 );
}

/* Add data to column in admin. */
function fill_integration_column( $colname, $post_id )
{
	$taxonomy = 'integration_cat';
	if ( $colname === $taxonomy )
	{
		$terms = wp_get_post_terms( $post_id, $taxonomy );
		foreach ( $terms as $term ) echo $term->name . '<br>';
	}
}

/* Make column sortable */
function add_integration_sortable_column( $sortable_columns )
{
	$taxonomy = 'integration_cat';
	$sortable_columns[ $taxonomy ] = [ $taxonomy, false ];
	return $sortable_columns;
}

add_filter( 'manage_' . 'integration' . '_posts_columns', 'add_integration_column', 4 );
add_action( 'manage_' . 'integration' . '_posts_custom_column', 'fill_integration_column', 5, 2 );
add_filter( 'manage_edit-' . 'integration' . '_sortable_columns', 'add_integration_sortable_column' );