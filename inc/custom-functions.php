<?php
/**
 * Custom functions
 */

/**
 * Add a breadcrumb
 */
function the_breadcrumb(){
 
	// получаем номер текущей страницы
	$pageNum = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
   
	$separator = ''; //  »
   
	// если главная страница сайта
	if( is_front_page() ){
   
	  if( $pageNum > 1 ) {
		echo '<a href="' . site_url() . '">' . 'main' . '</a>' . $separator . $pageNum . ' page';
	  }
   
	} else { // не главная
   
	  echo '<a href="' .site_url(). '">' . get_bloginfo('name') . '</a>' . $separator; 
   
	  if( is_single() ){ // записи
   
		the_category( $separator, 'multiple'); echo $separator; the_title();
   
	  } elseif ( is_home() ){
			   
			  wp_title('');
   
	  } elseif ( is_page() ){
   
		global $post;

			  if ( $post->post_parent ) {
  
				  $parent_id  = $post->post_parent;
				  $breadcrumbs = array(); 
  
				  while ( $parent_id ) {
					  $page = get_page( $parent_id );
					  $breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
					  $parent_id = $page->post_parent;
				  }
				  echo join( $separator, array_reverse( $breadcrumbs ) ) . $separator;
			  }
			  
			  the_title();
   
	  } elseif ( is_category() ) {
			  
			  $current_cat = get_queried_object();
			  if( $current_cat->parent ) {
				  echo get_category_parents( $current_cat->parent, true, $separator ) . $separator;
			  }
   
		single_cat_title();
   
	  } elseif( is_tag() ) {
   
		single_tag_title();
   
	  } elseif ( is_day() ) {
   
		echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
		echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $separator;
		echo get_the_time('d');
   
	  } elseif ( is_month() ) {
   
		echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
		echo get_the_time('F');
   
	  } elseif ( is_year() ) {
   
		echo get_the_time('Y');
   
	  } elseif ( is_author() ) {
   
		global $author;
		$userdata = get_userdata($author);
		echo 'Опублікував(ла) ' . $userdata->display_name;
   
	  } elseif ( is_404() ) {
  
		echo 'Помилка 404'; 
	  }
   
	  if ( $pageNum > 1 ) {
		echo ' (' . $pageNum . '-я страница)';
	  }
    }
}

/**
 * Messengers links
 */
function the_messenger_link( $messenger = '' )
{
    $phone = get_sub_field( $messenger );
    $icon  = get_bloginfo( 'template_url' ). '/img/' .$messenger . '.svg';

    switch ( $messenger ) {
        case 'telegram' :
            $link = 'https://t.me/'. $phone;
            break;

        case 'viber' :
            if ( wp_is_mobile() && ! preg_match( '/iPhone|iPad|iPod/i', $_SERVER ['HTTP_USER_AGENT'] ) ) {
                $viber_prefix = "";
            } else {
                $viber_prefix = "%2B";
            }

            $link = 'viber://chat?number='. $viber_prefix . preg_replace('/[^0-9]/', '', $phone );
            break;

        case 'whatsapp' :
            $link = 'https://wa.me/'. preg_replace('/[^0-9]/', '', $phone );
            break;
    };

    printf(
        '<a href="%s">
	        <img src="%s" alt="%s">
	    </a>',
        $link,
        $icon,
        $messenger
    );
}


/**
 * Posts loop
 *
 * @param array $args
 * @param string $slider
 */
function the_post_loop( $args = [], $slider = '' )
{
	$args['post_type'] = $args['post_type'] ? : 'post';

    $wp_query = new WP_Query( $args );

    if( $wp_query->have_posts() ) {
	    while ( $wp_query->have_posts() ) {
		    $wp_query->the_post();
		    get_template_part( 'template-parts/content', $args['post_type'], ['slider' => $slider] );
	    }

	    the_posts_pagination();

    } else {
	    get_template_part( 'template-parts/content', 'none' );
    }

    wp_reset_postdata();
}


/**
 * Option pages
 *
 * Add option pages using Advanced Custom Fields
 */
if( function_exists('acf_add_options_page') )
{
    acf_add_options_page([
        'page_title'  => 'Main contact info',
        'menu_title'  => 'Contact info',
        'menu_slug'   => 'main-contact-info',
        'capability'  => 'edit_posts',
        'post_id'     => 'contact',
        'icon_url'    => 'dashicons-phone',
        'position'    => 29,
    ]);
}

/**
 * ACF button
 *
 * @param string $type - case of "term" or "page"
 */
function acf_button( $type = '', $field = '', $class = '' )
{
	if ( ! get_field( $field ) ) return;

    if( empty( $id ) ) $id = get_the_ID();

    if( have_rows( $field, $id ) ) {
        while( have_rows( $field, $id ) ) {
            the_row();

            $text = get_sub_field('text');

            switch($type) {
                case 'term' :
                    $term = get_sub_field('link');
                    $link = get_term_link( $term->term_id, $term->taxonomy );
                    break;
                case 'page' :
                    $link = get_sub_field( 'link' );
                    break;
            };

            printf(
                '<a href="%s" class="%s">%s</a>',
                $link,
                $class,
                $text
            );
        }
    }
}

/* ACF Gallery */
function acf_gallery( $images, $lightbox = false )
{
    $size = 'large'; // (thumbnail, medium, large or custom size)

    if( $images ) {

        foreach ($images as $image) {

            if( $lightbox ) {
                printf(
                    '<a data-fslightbox="%s" href="%s">
                        <img src="%s" alt="%s">
                    </a>',
                    $image['url'],
                    $image['sizes'][$size],
                    $image['alt']
                );
            } else {
                printf(
                    '<img src="%s" alt="%s">',
                    $image['sizes'][$size],
                    $image['alt']
                );
            }
        }
    }
}

/**
 * Contact info
 */
function lr_contact_info( $field, $before = '', $after = '' )
{
	if ( !$field ) return;

	switch( $field ) {

		case 'email' :
			$email = get_field('email', 'contact');
			echo $before .'<a href="mailto:'. $email .'">'. $email .'</a>'. $after;
			break;

		case 'phones' :
			if ( have_rows('phones', 'contact') ) {
				while ( have_rows('phones', 'contact') ) {
					the_row();
					$phone = get_sub_field('phone');
					echo $before .'<a href="tel:'. $phone .'">'. $phone .'</a>'. $after;
				}
			}
			break;
	};
}


/**
 * Get background image
 */
function lr_background() {
    if( is_home() ) {
        $background = get_the_post_thumbnail_url( get_option('page_for_posts') );
    } elseif ( has_post_thumbnail() ) {
        $background = get_the_post_thumbnail_url();
    } else {
        $background = get_header_image();
    }    
    return $background;
}

/**
 * Get title
 */
function lr_title() {
    if ( is_front_page() ) {
        $title = get_bloginfo('name');
    } elseif ( is_home() ) {
        $title = get_the_title( get_option('page_for_posts') );
    } elseif ( is_archive() ) {
        $title = get_the_archive_title();
    } else {
        $title = get_the_title();
    }
    return $title;
}