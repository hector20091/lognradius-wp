<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package loginradius
 */

get_template_part('components-important/footers/full' );

wp_footer(); ?>

</body>
</html>
