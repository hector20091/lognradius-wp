<?php
/**
 * Template part for displaying section with testimonials
 */

$section_title = $args['title'];
$testimonials  = new WP_Query([
	'post_type' => 'testimonial'
]);
?>

<section class="bg-light-01 ws testimonials" id="testimonials">
    <div class="grid-100">
        <div class="title">
            <h2><?= $section_title ?></h2>
        </div>
    </div>

    <div class="grid-50">

	    <?php
            while ( $testimonials->have_posts() ) {
                $testimonials->the_post();
                get_template_part('components-single/testimonial');
            }
            wp_reset_postdata();
	    ?>

    </div>
</section>