<section class="ws bg-light-01" id="comparison">

    <!-- section title -->
    <div class="grid-100">
        <div class="title">
            <h2>
                Compare
            </h2>
        </div>
    </div>


    <div class="grid-100">
        <div>


            <div class="lr-table-style dragscroll comparison three-items">
                <table>

                    <thead>
                        <tr>
                            <th>
                                <a class="expand-all">
                                    <span>COLLAPSE ALL</span>
                                    <div class="icon">
                                        <span class="accordion-moving-part collapseAccordion"></span>
                                        <span></span>
                                    </div>
                                </a>
                            </th>
                            <th><a href="#trig-freeProducts">Free</a></th>
                            <th><a href="#trig-fullPlatform">Developer</a></th>
                            <th><a href="#trig-fullPlatform">Developer Pro</a></th>
                        </tr>
                    </thead>


                </table>



                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Authentication</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table style="display: none;">
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Standard Login</span>
                                <span class="tooltip">User registration and login using unique ID (email address or username) and password.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Phone Login</span>
                                <span class="tooltip">User registration and login using a phone number (per SMS cost not included).</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Password-less via SMS</span>
                                <span class="tooltip">User registration and login using an OTP sent to the phone number (per SMS cost not included).</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Password-less via Email</span>
                                <span class="tooltip">User registration and login via a link sent to the registered email address.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Social Login</span>
                                <span class="tooltip">User registration and login using social accounts, including Facebook, LinkedIn, Twitter, and 35 more.</span>
                            </td>
                            <td>3 Social Login Providers</td>
                            <td>5 Social Login Providers</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Email Template and Workflow</span>
                                <span class="tooltip">Pre-defined email templates that are easy to customize for all applicable workflows in the langauage of your choice.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>SMS Template and Workflow</span>
                                <span class="tooltip">Pre-defined SMS templates that are easy to customize for all applicable workflows in the langauage of your choice.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>

                    </tbody>
                </table>
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Identity Experience (IDx) Framework</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table style="display: none;">
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Hosted IDx</span>
                                <span class="tooltip">Ready-to-use web pages available on a dedicated instance of LoginRadius, which has already set up registration, login, user profile, and forgot password processes. Multiple page templates are available making it easy to customize the UI and UX.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Self-Hosted IDx</span>
                                <span class="tooltip">Ready-to-use identity pages (registration, login, forgot password, and user profile) that you can customize using our build-in JavaScript template engine and host on your domain.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Pre-designed Templates</span>
                                <span class="tooltip">Multiple fully customizable templates for the Hosted Identity Experience Framework (IDX).</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Custom Logo and Branding</span>
                                <span class="tooltip">Fully customizable layouts and branding enabling alignment with your branding guidelines and themes.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Custom Messaging</span>
                                <span class="tooltip">Full content control for all consumer facing content and messaging </span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Single Sign-on (SSO)</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table style="display: none;">
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Web and Mobile SSO</span>
                                <span class="tooltip">Seamless authentication among multiple applications for your consumers.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Account Linking</span>
                                <span class="tooltip">Streamline consumer experience via account linking even if the customer uses various login providers for your application.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Role based Authorization</span>
                                <span class="tooltip">Role and access permission management for individual consumers.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Directory Service</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Customer Data Storage</span>
                                <span class="tooltip">Host your users’ data at the preferred geographic location with a cloud-based scalable, secure, and high-performance data repository. </span>
                            </td>
                            <td>Choice of 1 data center</td>
                            <td>Choice of 1 data center</td>
                            <td>Choice of 1 data center</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Customer Profiling</span>
                                <span class="tooltip">Better understand your application consumers with detailed consumer metadata such as demographic, age group, preferences, etc.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Progressive Data Gathering</span>
                                <span class="tooltip">Gradual data collection of your consumers as they interact with your brand.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Account Security</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Spam Prevention</span>
                                <span class="tooltip">Access restriction on suspicious email domains to prevent spam registrations.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Brute Force Lockout</span>
                                <span class="tooltip">Protect consumers against brute force attacks by prompting either security questions, CAPTCHA or suspending/blocking an account.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Password Policy</span>
                                <span class="tooltip">Enhance security using password parameters such as expiration time, history, complexity, encryption, and more.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Session Management</span>
                                <span class="tooltip">Configure active session settings for your application.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Global CAPTCHA</span>
                                <span class="tooltip">Bot protection using Google ReCAPTCHA and/or QQ CAPTCHA.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Data and Identity Migration</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Mapping data to LR Database</span>
                                <span class="tooltip">Granular field mapping from your existing data set to LoginRadius Cloud Database.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>De-duplication</span>
                                <span class="tooltip">Duplicate profile removal during data migration.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Data Normalization</span>
                                <span class="tooltip">Transforms data into the LoginRadius Normalized Consumer Profile Format.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Data Sanitation</span>
                                <span class="tooltip">Various actions ensuring the data migrated into your LoginRadius Cloud database follows best practices and is formatted correctly including: field-level preprocessing, required field verification, precedential data merging, data filtering and more.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Password Reset Not Required</span>
                                <span class="tooltip">Migrate existing hashed passwords to avoid reseting consumer passwords after the data migration is completed.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Trigger email content</span>
                                <span class="tooltip">Automatically trigger emails to your consumers during/after the data migration.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Enforce password reset</span>
                                <span class="tooltip">Enforce consumers to reset their account passwords on first login or based on defined criteria</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Multiple Hash Supported</span>
                                <span class="tooltip">Multiple hashing algorithms supported for migration including PBKDF2, SHA, MD5, etc.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Multiple Data Source</span>
                                <span class="tooltip">Migrate from multiple data sources simultaneoulsy.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Migration from other CIAM Platform</span>
                                <span class="tooltip">Standardized process to migrate from other industry CIAM providers.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>CSV based migration</span>
                                <span class="tooltip">Migrate your data using the CSV files.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Self-serve migration</span>
                                <span class="tooltip">Self-serve data migration using LoginRadius Admin Console.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Cloud Hosting</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Regional Data Hosting</span>
                                <span class="tooltip">Data is hosted in your selected location. Refer to Product Add-On section for more details on the regional database.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Data Back-up Service</span>
                                <span class="tooltip">Incremental and full data backups.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Disaster Recovery Service</span>
                                <span class="tooltip">Support disaster recovery of data.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Data Security</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Crypto Hashing</span>
                                <span class="tooltip">Support industry recommended hashing algorithms to protect sensitive information.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Encryption in Transit</span>
                                <span class="tooltip">All information is encrypted while in transit.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Encryption at Rest</span>
                                <span class="tooltip">All information is encrypted at rest in the database.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>System Performance</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Uptime SLA</span>
                                <span class="tooltip">Our service contract guarantees system availability of 99.99% uptime on a monthly basis.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>99.99% Monthly Availability</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Privacy</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>GDPR &amp; CCPA</span>
                                <span class="tooltip">Provide GDPR and CCPA compliance tools and resources.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Age Verification (Restriction)</span>
                                <span class="tooltip">Verify the consumers age during registration or authentication.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Compliances</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>LR Compliances</span>
                                <span class="tooltip">We are SOC2, ISO27001, ISO 27001:2013, ISO 27017, 27018, NIST CSF, CSA Star, Privacy Shield, and ISAE 3000-certified. </span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>ISO 27001</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Cloud Compliances</span>
                                <span class="tooltip">We are ISO 27017, 27018, CSA Star, and Privacy Shield cloud certified.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Customer Data Gathering</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Registration data</span>
                                <span class="tooltip">Collect first-party data through your registration form.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Social data</span>
                                <span class="tooltip">Collect first-party data through social networks with consent from consumers.</span>
                            </td>
                            <td>Basic</td>
                            <td>Extended</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Technical Support</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>24/7 Email Support</span>
                                <span class="tooltip">24/7 email support from LoginRadius engineers.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Live Chat</span>
                                <span class="tooltip">Live chat support from LoginRadius engineers.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Support SLA</span>
                                <span class="tooltip">24/7 SLA with guarenteed response times based on priority classification.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>Add-on</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Professional Services</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Dedicated Account Manager</span>
                                <span class="tooltip">Dedicated Account Manager appointed to ensure the success during your implementation and ongoing usage of the LoginRadius CIAM platform.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Onboarding (Account set, training, implementation support)</span>
                                <span class="tooltip">A Dedicated team to support your initial LoginRadius deployment. Facilitates account setup, Training of your team and ongoing coordination to ensure a quick seamless deployment of the LoginRadius CIAM platform.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td>Self-serve</td>
                            <td>Basic</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>User Migration</span>
                                <span class="tooltip">Migrate your existing consumers to LoginRadius using our user migration software.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td>Self-serve</td>
                            <td>Add-on</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                 
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Team Management</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Roles and permissions</span>
                                <span class="tooltip">Granular access level management for your internal team members </span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>SSO with internal IAM</span>
                                <span class="tooltip">Enable SSO with your employee IAM to give them access to the LoginRadius Admin Console.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Employee Audit Trail</span>
                                <span class="tooltip">Additional security layer to track how your employees are interacting with the LoginRadius Admin Console</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>30 Days</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Platform</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Mobile SDKs</span>
                                <span class="tooltip">Mobile SDKs for all industry leading mobile technologies.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Web SDKs</span>
                                <span class="tooltip">Web SDKs for all industry leading programming languages.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>API Key and Secret</span>
                                <span class="tooltip">API Key and Secret authorization for all API endpoints.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Data Sync (Web Hooks )</span>
                                <span class="tooltip">Sync consumer data with your external applications, eliminate data silos and ensure data is distributed to relying applications.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Custom Domain</span>
                                <span class="tooltip">Whitelist your domain to display your URL for all LoginRadius Domains.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Self-serve Implementation</span>
                                <span class="tooltip">Easy-to-follow implementation guides and support documentation to get up-and-running quickly and easily.</span>
                            </td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                            <td class="check">✓</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Product Add-ons</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Federated SSO App</span>
                                <span class="tooltip">Give your partners seamless access to your web applications through your preferred protocol – (SAML, JWT, Multipass, etc.)</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>$500 / Mo / 3 Apps</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Two-Factor Authentication</span>
                                <span class="tooltip">Verify account access through SMS or an authenticator application. Backup codes, account recovery and other standard MFA processes are included. </span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>$417 / Mo</td>
                        </tr>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Custom Object (Each)</span>
                                <span class="tooltip">Store complex, schemaless consumer metadata based on your sites requirements.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>$417 / Mo</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                <a class="accordion-head">
                    <div class="accordion-head-bg"></div>

                    <p>Cloud Connector (Integration)</p>
                    <div class="icon">
                        <span class="accordion-moving-part collapseAccordion"></span>
                        <span></span>
                    </div>
                </a>


                <table>
                    <tbody>
                        <tr class="bg-light-01">
                            <td class="tooltip-container">
                                <span>Native LR integrations (one-way data sync)</span>
                                <span class="tooltip">Integrate LoginRadius’ data and identity with any third-party application such as CRM, analytics, payment gateway, etc.</span>
                            </td>
                            <td class="check-x">✗</td>
                            <td class="check-x">✗</td>
                            <td>$292 / Mo / Integration</td>
                        </tr>
                    </tbody>
                </table>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            </div>
        </div>
    </div>





</section>