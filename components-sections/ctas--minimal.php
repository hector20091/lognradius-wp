<?php
/**
 * Template parts that displaying call to action section
 */

$section_title    = get_sub_field('title');      // string
$section_text     = get_sub_field('text');       // string
$section_button_1 = get_sub_field('button_1');   // array( 'url', 'text' )
$section_button_2 = get_sub_field('button_2');   // array( 'url', 'text' )
?>

