<?php
/**
 *
 */
?>


<section class="section logos-sphere">
    <div>
        <div class="line-1">
            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/carnival.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/carnival.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/7eleven.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/7eleven.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/mcpherson.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/mcpherson.png">
                </div>
            </div>
        </div>

        <div class="line-2">
            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/gmg.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/gmg.png">
                </div>

                <div class="cards-overlay hide-for-now">
                    <div class="close"></div>

                    <div class="cards-showcase-v2 alone">

                        <div>
                            <div class="image customer--tiroler-tageszeitung"></div>

                            <div class="heading">
                                <h4>Tiroler Tageszeitung</h4>
                            </div>

                            <div class="text">
                                <p>A long description is a way to provide long alternative text for non-text elements, such as images. Examples of suitable use of long description are charts, graphs, maps, infographics, and other complex images. Like alternative text, long description should be descriptive and meaningful. Wave will give you a warning when the alt text is over 100 characters. In some cases an alt text over 100 characters is appropriate. It will depend on the complexity of the image and where the images is used related to the content on the webpage.</p>
                            </div>

                            <div class="actions">
                                <a href="/resource/case-study-page-tiroler-tageszeitung/">read</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/partner-fusion.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/partner-fusion.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/ahold-delhaize.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/ahold-delhaize.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/arte.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/arte.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/ascend.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/ascend.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/avista.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/avista.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/baxter.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/baxter.png">
                </div>
            </div>

        </div>

        <div class="line-3">

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/mannings.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/mannings.png">
                </div>
            </div>



            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/mapegy.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/mapegy.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/conde-nast.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/conde-nast.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/cbc-news.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/cbc-news.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/olympic.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/olympic.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/dfw.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/dfw.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/rede-gazeta.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/rede-gazeta.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/georgia-tech.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/georgia-tech.png">
                </div>
            </div>

        </div>

        <div class="line-4">
            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/events.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/events.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/fox.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/fox.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/forex-club.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/forex-club.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/iom.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/iom.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/jpm.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/jpm.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/treasure-wine-estates.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/treasure-wine-estates.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/dairy-farm.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/dairy-farm.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/radiopolis.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/radiopolis.png">
                </div>
            </div>

        </div>

        <div class="line-center">

            <div class="popup">
                <a class="content" href="/resource/case-study-page-tiroler-tageszeitung">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/tiroler-tageszeitung.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/tiroler-tageszeitung.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-zachys">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/zachys.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/zachys.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-harry-rosen">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/harry-rosen.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/harry-rosen.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-express-star">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/express-and-star.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/express-and-star.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-c-quence">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/cquence.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/cquence.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-kicksusa/">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/kicksusa.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/kicksusa.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-pelmorex/">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/pelmorex.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/pelmorex.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-hola/">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/hola.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/hola.png">
                </a>
            </div>

        </div>

        <div class="line-center">

            <div class="popup">
                <a class="content" href="/resource/case-study-page-hydro-ottawa">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/hydra-ottawa.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/hydra-ottawa.png">
                </a>
            </div>

            <div class="popup">
                <a class="content" href="/resource/case-study-page-safebridge">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/safebridge.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/safebridge.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-zero-waste-scotland">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/zerowaste.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/zerowaste.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-swann">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/swann.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/swann.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-city-of-surrey">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/city-of-surrey.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/city-of-surrey.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-juhll">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/juhll.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/juhll.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-broadcastmed">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/broadcast-med.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/broadcast-med.png">
                </a>
            </div>


            <div class="popup">
                <a class="content" href="/resource/case-study-page-bauer-media">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/bauer.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/bauer.png">
                </a>
            </div>

        </div>

        <div class="line-4">

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/starbucks.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/starbucks.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/itv.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/itv.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/tui.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/tui.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/tvp.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/tvp.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/ulta-beauty.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/ulta-beauty.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/under-armour.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/under-armour.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/kind.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/kind.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/suit-supply.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/suit-supply.png">
                </div>
            </div>

        </div>

        <div class="line-3">

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/ucla.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/ucla.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/travelpass-group.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/travelpass-group.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/tone-it-up.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/tone-it-up.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/trx.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/trx.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/upmc.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/upmc.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/onename.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/onename.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/spirit.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/spirit.png">
                </div>
            </div>



            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/britbox.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/britbox.png">
                </div>
            </div>

        </div>

        <div class="line-2">

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/nmw.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/nmw.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/weather-network.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/weather-network.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/aurora.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/aurora.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/world-vision.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/world-vision.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/nasdaq.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/nasdaq.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/gnc.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/gnc.png">
                </div>
            </div>


            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/sd-zoo.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/sd-zoo.png">
                </div>
            </div>

        </div>

        <div class="line-1">

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/21st-century-fox.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/21st-century-fox.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/world-economic.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/world-economic.png">
                </div>
            </div>

            <div>
                <div class="content">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/logos/customers-color/ifma.png" class="large" src="/wp-content/themes/login-radius/images/logos/customers-color/ifma.png">
                </div>
            </div>

        </div>

    </div>

</section>