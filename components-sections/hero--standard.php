<?php
/**
 * Template part for displaying basic hero section
 */

$position         = get_sub_field('image_position');    // left / right
$image            = get_sub_field('image');             // array( 'url', 'atl', 'title' )
$type             = get_sub_field('type');              // string
$section_title    = get_sub_field('section_title');     // string
$section_subtitle = get_sub_field('section_subtitle');  // string
$text             = get_sub_field('text');              // string
$button           = get_sub_field('button');            // array( 'url', 'text' )
$logos_list       = get_sub_field('logos');             // array()
$bg_circle        = get_sub_field('background_circle'); // circle1 / circle2 / circle3 / circle4
?>

<section class="bg-light-01 hero-standard">
    <div class="grid-50">
        <div>
            <h1><?= $section_title ?></h1>
            
            <ul class="bullets">
                <li>No code knowledge needed</li>
                <li>Secure with many compliances</li>
                <li>Unmatched performance</li>
                <li>Incredible speed of deployment</li>
            </ul>
            
            <a href="https://accounts.loginradius.com/auth.aspx?action=register" class="btn-primary">Free Sign Up</a>
        </div>

        <div>
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/key-benefits/dashboard.png" class="photo lazyloaded" src="/wp-content/themes/login-radius/images/ws/key-benefits/dashboard.png">
            
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/bg-circle.svg" class="bg-circle circle1 lazyloaded" src="/wp-content/themes/login-radius/images/ws/bg-circle.svg">
        </div>
    </div>
</section>