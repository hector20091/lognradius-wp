<?php
/**
 * Template part for displaying section with text content on "case study" template for "resource" post type
 *
 * @param array $args = [ 'content', 'id' ]
 */

$content    = $args['content'];
$section_id = $args['id']
?>

<section class="bg-dark-02 case-study-page" id="<?= $section_id ?>">
    <div class="grid-100">
        <div>
            <?= $content ?>
        </div>
    </div>
</section>