<?php
/**
 * Template part for displaying section with nutshells
 */

$section_title    = get_sub_field('section_title');     // string
$section_subtitle = get_sub_field('section_subtitle');  // string
$title_size       = get_sub_field('title_size');        // 3 columns/4 columns
$nutshells_list   = get_sub_field('nutshells');         // array()
?>