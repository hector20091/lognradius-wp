<?php
/**
 * Template parts that displaying call to action section
 */
?>

<section class="bg-light-01 footer-cta">
    <div class="grid-100">
        <div class="title">
            <h2>Authentication &amp; SSO Made Simple</h2>
            <p>Easily add authentication and SSO to any web and mobile applications in 5 minutes.</p>
            <a href="https://accounts.loginradius.com/auth.aspx?action=register" class="btn-primary sign-up-button">Free Sign Up</a>
        </div>
    </div>
</section>