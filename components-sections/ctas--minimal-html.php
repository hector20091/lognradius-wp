<?php
/**
 * Template parts that displaying call to action section
 */

$section_title    = get_sub_field('title');     // string
$section_subtitle = get_sub_field('subtitle');  // string
$section_content  = get_sub_field('text');      // string
?>

