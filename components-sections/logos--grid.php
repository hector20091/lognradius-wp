<?php
/**
 * Template part for displaying section with grid of logos list
 *
 * @param array $args = [ 'img' => ['url','alt','title'], 'link' => ['url','text'] ]
 */

$cards_list = $args['cards'];
?>

<section class="logos-grid">

    <?php foreach ( $cards_list as $card ) : ?>

        <div style="background-color:<?= $card['bg_color'] ?>">
            <a class="content" <?= $card['link'] ? "href='{$card['link']['url']}'" : null ?>>
                <img
                    class="large lazyloaded"
                    alt="<?= $card['img']['alt'] ?>"
                    src="<?= $card['img']['url'] ?>"
                    data-src="<?= $card['img']['url'] ?>"
                >
            </a>
        </div>

    <?php endforeach; ?>

</section>