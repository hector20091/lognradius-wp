<?php
/**
 * Template part for displaying hero section on "case study" template for "resource" post type
 */

$hero_title    = get_sub_field('title');                // string
$hero_subtitle = get_sub_field('subtitle');             // string
$image         = get_sub_field('image');                // array( 'url', 'alt', 'title' )
$button_after  = get_sub_field('button');               // array( 'url', 'text' )
$sections_list = get_sub_field('table_of_contents');    // array()
?>

<section class="bg-dark-02 case-study-page case-study--city-of-surrey">
    <div class="grid-100 title hero">
        <div>
            <p class="resource-type">CASE STUDY</p>
            <h1><?= $hero_title ?></h1>
            <div class="subhead">
                <p><?= $hero_subtitle ?></p>
            </div>
            <p class="btn-adjacent margin-top--md">
                <a href="/contact-sales" class="btn-primary">Contact Sales</a>
                <a href="/book-a-demo" class="btn-secondary">Book a Demo</a>
            </p>
        </div>
    </div>

    <div class="grid-100 hyperlink-nav"><hr>
        <p>
            <?php foreach ( $sections_list as $section ) : ?>

                <a href="#about"><?= $section['title'] ?></a> <span class="separator">|</span>

            <?php endforeach; ?>
        </p>
    </div>
</section>