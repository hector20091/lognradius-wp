<?php
/**
 * Template parts that displaying call to action section
 */
?>

<section class="bg-light-01 footer-cta">
    <div class="grid-100">
        <div class="title">
            <h2>LoginRadius Identity Platform</h2>
            <h3>Built for engineers, by engineers.</h3>
            <p>Our developer-friendly platform is built with extensive API documentation, open-source SDKs, fully customizable interfaces, and best-in-class data security features.</p>
            <p>Ready to give it a spin?</p>
            <a class="btn-primary" href="/contact-sales/">Contact Us</a>
            <p>Or call us at <a href="tel:18446258889">+1-844-625-8889</a></p>
        </div>
    </div>
</section>
