<?php
/**
 * Template part for displaying minimal hero section
 */

$background_color = get_sub_field('bg_color');   // hex color
$section_title    = get_sub_field('title');      // string
$section_subtitle = get_sub_field('subtitle');   // string
$section_text     = get_sub_field('text');       // string
$section_button   = get_sub_field('button');     // array( 'url', 'text' )
?>
