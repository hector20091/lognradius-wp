<?php
/**
 *
 */

$section_title   = get_sub_field('section_title');      // string
$amount_of_logos = get_sub_field('title_size');         // select in [1 - 10]
$logo_image      = get_sub_field('section_subtitle');   // array( 'url', 'alt', 'title' )

?>

<section class="subsection logos-scrolling">
    <div>

        <a href="/customers">


            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/7eleven.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/7eleven.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/cbc-news.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/cbc-news.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/city-of-surrey.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/city-of-surrey.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/express-and-star.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/express-and-star.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/ulta-beauty.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/ulta-beauty.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/zerowaste.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/zerowaste.png">
            </div>


            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/weather-network.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/weather-network.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/safebridge.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/safebridge.png">
            </div>



        </a>


        <a href="/customers">


            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/7eleven.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/7eleven.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/cbc-news.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/cbc-news.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/city-of-surrey.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/city-of-surrey.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/express-and-star.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/express-and-star.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/ulta-beauty.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/ulta-beauty.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/zerowaste.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/zerowaste.png">
            </div>


            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/weather-network.png" class="large" src="/wp-content/themes/login-radius/images/logos/logo-with-colors/weather-network.png">
            </div>



            <div>
                <img alt="" data-src="/wp-content/themes/login-radius/images/logos/logo-with-colors/safebridge.png" class="large" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
            </div>



        </a>

    </div>
</section>