<section class="bg-light-01 sidekick">
    
    <div class="grid-50">


        <div>
            <h2>Step 1</h2>
            
            <p>
                Sign up for one of our projects or our full platform
            </p>
            
            
            
            <a href="/pricing" class="btn-secondary">Full product comparison and pricing</a>
            <a href="https://accounts.loginradius.com/auth.aspx?action=register" class="btn-primary">Free Sign Up</a>
        </div>
        <div>
            
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/homepage-step1.png" class="photo lazyloaded" src="/wp-content/themes/login-radius/images/ws/homepage-step1.png">
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/bg-circle.svg" class="bg-circle circle4 lazyloaded" src="/wp-content/themes/login-radius/images/ws/bg-circle.svg">

        </div>
        
    </div>
    
    
    
    
</section>