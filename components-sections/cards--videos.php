<?php
/**
 * Template part for displaying section with videos
 */

$section_title    = get_sub_field('title');        // string
$section_subtitle = get_sub_field('subtitle');     // string
$section_text     = get_sub_field('text');         // string
$section_button   = get_sub_field('button_after'); // array( 'url', 'text' )
$videos_list      = get_sub_field('videos');       // array()
?>