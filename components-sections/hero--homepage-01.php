<?php
/**
 * Template part for displaying hero section on homepage
 */
?>

<section class="bg-dark-01 padding-bottom--md padding-top--md home-page--signup-hero">
    <div class="grid-50 center-vertically margin-top--md margin-bottom--md ">
        <div>
            <h1>Some title text</h1>

            <h4 class="margin-top--md type-text-wrapper">
                No-code

                <div class="type-text">
                    <p class="p5">Privacy Management</p>
                    <p class="p4">Account Security</p>
                    <p class="p3">Federation</p>
                    <p class="p2">Authorization</p>
                    <p class="p1">Authentication</p>
                </div>
            </h4>

            <div class="btn-row-text-under margin-top--lg">
                <div>
                    <a href="/book-a-demo" class="btn-primary">REQUEST DEMO</a>
                
                    <a href="https://accounts.loginradius.com/auth.aspx?plan=developer" class="btn-secondary">TRY IT FOR FREE</a>
                </div>
                
                <p class="text">or <a href="/contact-sales">Contact Sales</a></p>
            </div>
        </div>

        <div>
            <img alt="" data-src="/wp-content/themes/login-radius/images/heroes/dashboard-anim.gif" class="anim-gif lazyloaded" src="/wp-content/themes/login-radius/images/heroes/dashboard-anim.gif">
        </div>

    </div>
</section>