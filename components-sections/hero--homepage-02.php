<?php
/**
 * Template part for displaying hero section on homepage
 */
?>

<section class="bg-light-01 ws home-hero">

    <div class="grid-50">
        <div>
            <h1>Add Login to Your Application in 5 Minutes!</h1>

            <ul class="checkmarks">
                <li>The no-code solution of choice for developers and large enterprises</li>
                <li>Advanced user management</li>
                <li>Industry-leading security</li>
                <li>150+ integrations</li>
            </ul>
        </div>

        <div>


            <video poster="/wp-content/themes/login-radius/videos/heros/hero3-f1.png" class="lr-video-style" loop="" muted="" autoplay="" preload="auto" src="/wp-content/themes/login-radius/videos/heros/hero3-fast.mp4">
            </video>
        </div>
    </div>

    <div class="grid-100">
        <div>

            <div id="form-home-1" class="lr-form-small">
                <div class="error-message">Please enter a valid email.</div>
                <form data-dashlane-rid="1cf13314651c8c67" data-form-type="newsletter">
                    <fieldset><input type="text" name="emailField" required="" data-dashlane-rid="c3208f0d1fcc987a" data-kwimpalastatus="alive" data-kwimpalaid="1637019744343-0" data-form-type="email"><label for="emailField">Enter your email</label><input type="submit" value="Sign Up" data-dashlane-rid="d1184f5ee5f4de6e" data-form-type="action,subscribe"></fieldset>
                </form>
            </div>

            <div class="social-icons">

                <div class="separator">
                    <p>or</p>
                </div>

                <a href="#">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/ws/social-icons/google.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/ws/social-icons/google.svg">
                    <div class="loader">
                        <div></div>
                    </div>
                </a>

                <a href="#">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/ws/social-icons/github.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/ws/social-icons/github.svg">
                    <div class="loader">
                        <div></div>
                    </div>
                </a>

                <a href="#">
                    <img alt="" data-src="/wp-content/themes/login-radius/images/ws/social-icons/facebook.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/ws/social-icons/facebook.svg">
                    <div class="loader">
                        <div></div>
                    </div>
                </a>
            </div>
        </div>

        <div>
            <p class="faded">No credit card required. No hidden fees.</p>
        </div>
    </div>


</section>
