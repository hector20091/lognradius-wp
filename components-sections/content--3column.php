<?php
/**
 * Template part for displaying section with 3 column
 */
?>

<section class="ws content-3 bg-light-01">

    <!-- section title -->
    <div class="grid-100">
        <div class="title">
            <h2>
                3 blogs, each with unique exciting content
            </h2>
        </div>
    </div>


    <div class="grid-33">

        <div>
            <div class="image">
                <img data-src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-async.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-async.svg">
            </div>
            <div class="text">
                <p>
                    Find out the latest news in the world of engineering. All engineering topics are covered here.
                </p>
                
                <p class="margin-bottom--none">
                    <a class="btn-link-small" href="https://www.loginradius.com/blog/async/">VISIT BLOG</a>
                </p>
            </div>
        </div>

        <div>
            <div class="image">
                <img data-src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-start-with-identity.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-start-with-identity.svg">
            </div>
            <div class="text">
                <p>
                   Find out more about Identity and Access Management (IAM), including security and customer experience.
                </p>
                
                <p class="margin-bottom--none">
                    <a class="btn-link-small" href="https://www.loginradius.com/blog/start-with-identity/">VISIT BLOG</a>
                </p>
            </div>
        </div>

        <div>
            <div class="image">
                <img data-src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-fuel.svg" class=" lazyloaded" src="/wp-content/themes/login-radius/images/logos/blog-logos/blogLogo-fuel.svg">
            </div>
            <div class="text">
                <p>
                    How to grow your business to millions of customers? How to engage and retain your customers? All of these questions and more are answered in this blog.
                </p>
                
                <p class="margin-bottom--none">
                    <a class="btn-link-small" href="https://www.loginradius.com/blog/fuel/">VISIT BLOG</a>
                </p>
            </div>
        </div>

    </div>
</section>