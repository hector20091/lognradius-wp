<?php
/**
 * Template part for displaying basic hero section
 */

$section_title    = $args['title'];
$section_subtitle = $args['subtitle'];
$categories_list  = $args['categories'];
?>

<section class="integration-top">
    <div class="grid-100">
        <div class="title padding-bottom--sm">
            <h1><?= $section_title ?></h1>
            <p><?= $section_title ?></p>
        </div>
    </div>
</section>

<section class="integration-top-02 bg-light-01">
    <div class="grid-100">
        <div>
            <div id="searchbox-wrapper" class="hero-searchbox-wrapper">
                <div id="searchbox-frame">
                    <label>
                        <input class="searchbox" type="text" placeholder="Search our integrations">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-100">
        <div>
            <div class="integration-menus">
                <ul class="btn-list">
                    <li class="accordion-list">
                        <a class="anchor" tabindex="0"><p>Categories</p></a>
                    </li>

                    <?php foreach ( $categories_list as $category ) : ?>

                        <li class="item-btn">
                            <a class="btn-secondary" href="#<?= $category->slug ?>">
                                <?= $category->name ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
</section>