<section class="bg-light-01 sidekick alt">
    
    <div class="grid-50">
<div>
            
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/homepage-step2.png" class="photo lazyloaded" src="/wp-content/themes/login-radius/images/ws/homepage-step2.png">
            <img alt="" data-src="/wp-content/themes/login-radius/images/ws/bg-circle.svg" class="bg-circle circle1 ls-is-cached lazyloaded" src="/wp-content/themes/login-radius/images/ws/bg-circle.svg">

        </div>


        <div>
            <h2>Step 2</h2>
            
            <p>
                Configure your dashboard
            </p>
            
            
            
            <a href="/integrations-list" class="btn-secondary">All of our 150+ integrations</a>
            
            
            <div class="logos">
                <a href="/integrations/wordpress">
                    <img alt="wordpress logo" data-src="/wp-content/uploads/2015/04/wordpress.png" class=" lazyloaded" src="/wp-content/uploads/2015/04/wordpress.png">
                </a>
                
                <a href="/integrations/salesforce">
                    <img alt="salesforce logo" data-src="/wp-content/uploads/2019/04/salesforce.png" class=" lazyloaded" src="/wp-content/uploads/2019/04/salesforce.png">
                </a>
                
                <a href="/integrations/magento">
                    <img alt="magento logo" data-src="/wp-content/uploads/2019/04/magento.png" class=" lazyloaded" src="/wp-content/uploads/2019/04/magento.png">
                </a>
                
                <a href="/integrations/shopify">
                    <img alt="shopify logo" data-src="/wp-content/uploads/2019/04/shopify.png" class=" lazyloaded" src="/wp-content/uploads/2019/04/shopify.png">
                </a>
                
                <a href="/integrations/google-analytics">
                    <img alt="google-analytics logo" data-src="/wp-content/themes/login-radius/images/logos/logo-bar-integrations/google-analytics2.png" class=" lazyloaded" src="/wp-content/themes/login-radius/images/logos/logo-bar-integrations/google-analytics2.png">
                </a>
            </div>
            
        </div>
        
        
    </div>
    
    
    
    
    
    
    
    
</section>