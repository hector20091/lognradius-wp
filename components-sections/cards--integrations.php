<?php
/**
 * Template part for displaying section with graphic-images cards list
 *
 * @param array $args = [ 'title', 'category', 'count' ]
 */

$section_title = $args['title'];
$category      = $args['category']; // current
// get array of posts
$integrations  = new WP_Query([
	'post_type'       => 'integration',
    'integration_cat' => $category,
    'posts_per_page'  => $args['count']
]);
?>

<section class="integration-industry" id="<?= $category ?>">
    <div class="grid-100">
        <div>
            <div class="title">
                <h2><?= $section_title ?></h2>
            </div>

            <ul class="integration-blocks">
                <div>

	                <?php
                        while ( $integrations->have_posts() ) {
                            $integrations->the_post();
                            get_template_part('components-single/card--integration');
                        }
                        wp_reset_postdata();
	                ?>

                </div>
            </ul>
        </div>
    </div>
</section>