<?php
/**
 * Template part for displaying section with pricing cards
 */
?>

<div class="pricing-cards cards3">

    <?php get_template_part('/components-single/card--pricing-paid') ;?>

    <img alt="" data-src="/wp-content/themes/login-radius/images/ws/bg-circle.svg" class="bg-circle circle4 lazyloaded" src="/wp-content/themes/login-radius/images/ws/bg-circle.svg">
</div>
