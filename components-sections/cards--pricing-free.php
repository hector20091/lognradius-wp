<?php
/**
 * Template part for displaying section with pricing cards
 */
?>

<div class="pricing-cards cards2">

    <?php get_template_part('/components-single/card--pricing-free') ;?>

    <img alt="" data-src="/wp-content/themes/login-radius/images/ws/bg-circle.svg" class="bg-circle circle1 lazyloaded" src="/wp-content/themes/login-radius/images/ws/bg-circle.svg">
</div>
