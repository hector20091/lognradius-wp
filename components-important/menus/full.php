<nav>

    <div class="bar-mobile">
        <a tab-index="0" class="hamburger">
            <span></span>
            <span></span>
        </a>

        <a class="logo" href="/">
            <svg xmlns="http://www.w3.org/2000/svg" width="1200" height="246" viewBox="0 0 1200 246">
                <defs>
                    <style>
                        .dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf {
                            fill: #008ecf;
                        }

                        .hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb {
                            fill: #09263c;
                        }

                    </style>
                </defs>
                <title>LoginRadius Logo</title>
                <g id="horizontals">
                    <g>
                        <g>
                            <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M126.9,1.18A125.9,125.9,0,0,0,82.29,244.82l10.64-29.64a94.43,94.43,0,1,1,67.93,0l10.64,29.64A125.9,125.9,0,0,0,126.9,1.18Z" />
                            <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M103.57,185.54l10.65-29.67a31.47,31.47,0,1,1,25.36,0l10.65,29.67a63,63,0,1,0-46.66,0Z" />
                        </g>
                        <g>
                            <g>
                                <g>
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M739.19,95.72c1.57,0,3.38.09,5.42.29s4,.46,6,.79,3.78.66,5.43,1a33.88,33.88,0,0,1,3.64.88l-3.15,16a62.15,62.15,0,0,0-7.2-1.88,63.32,63.32,0,0,0-12.91-1.08,48.24,48.24,0,0,0-10.16,1.08c-3.35.73-5.56,1.22-6.61,1.48v86.18H701.33V102.22a125.18,125.18,0,0,1,16.17-4.43A103.81,103.81,0,0,1,739.19,95.72Z" />
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M803.49,95.32q11,0,18.63,2.86a30.21,30.21,0,0,1,12.23,8.09,30.61,30.61,0,0,1,6.6,12.42,60.2,60.2,0,0,1,2,15.88v64.08c-1.57.27-3.78.63-6.6,1.09s-6,.88-9.57,1.28-7.39.75-11.53,1.08-8.25.5-12.33.5a67.15,67.15,0,0,1-16-1.78,35.76,35.76,0,0,1-12.62-5.62A25.64,25.64,0,0,1,766,185.05a35.6,35.6,0,0,1-3-15.19,29.24,29.24,0,0,1,3.45-14.59,27.5,27.5,0,0,1,9.37-9.86,43.18,43.18,0,0,1,13.8-5.52,75.48,75.48,0,0,1,16.57-1.78,56.66,56.66,0,0,1,5.71.3q3,.3,5.62.79c1.78.33,3.32.62,4.64.89s2.23.46,2.76.59v-5.13a41.09,41.09,0,0,0-1-9,20.54,20.54,0,0,0-3.55-7.89,18,18,0,0,0-7-5.52,27.41,27.41,0,0,0-11.53-2.07A87.13,87.13,0,0,0,786,112.38,55.94,55.94,0,0,0,775.88,115l-2.17-15.18a56.9,56.9,0,0,1,11.83-3.06A101.79,101.79,0,0,1,803.49,95.32Zm1.57,91.9q6.51,0,11.54-.3a51.45,51.45,0,0,0,8.38-1.08V155.27a23.79,23.79,0,0,0-6.41-1.68,71.33,71.33,0,0,0-10.75-.69,68.41,68.41,0,0,0-8.77.6,26.86,26.86,0,0,0-8.48,2.46,18.22,18.22,0,0,0-6.41,5.13,13.45,13.45,0,0,0-2.56,8.57q0,9.87,6.31,13.71T805.06,187.22Z" />
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M929.17,45.37h18.34V200.43c-4.21,1.18-30.31,2.36-38.85,2.36a57.62,57.62,0,0,1-21.3-3.74,45.69,45.69,0,0,1-16.17-10.65,47,47,0,0,1-10.35-16.86,64.54,64.54,0,0,1-3.65-22.38,73,73,0,0,1,3.06-21.7,48.53,48.53,0,0,1,9-16.95,40.75,40.75,0,0,1,14.49-11.05,46.49,46.49,0,0,1,19.63-3.94A47.54,47.54,0,0,1,919,97.89a49.53,49.53,0,0,1,10.16,4.53Zm0,74.21a38,38,0,0,0-9.66-5.33,36.54,36.54,0,0,0-13.81-2.56,29.58,29.58,0,0,0-13.51,2.86,24.85,24.85,0,0,0-9.17,7.89,33.38,33.38,0,0,0-5.12,11.93,65.77,65.77,0,0,0-1.58,14.79q0,17.94,8.87,27.7c5.92,6.51,13.95,8.77,23.81,8.77,5,0,18.33-.65,20.17-1.17Z" />
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1129.67,187.22q11.24,0,16.66-3t5.42-9.47a12.58,12.58,0,0,0-5.32-10.65q-5.33-3.93-17.55-8.87-5.91-2.37-11.34-4.83a39.44,39.44,0,0,1-9.37-5.82,25.4,25.4,0,0,1-6.31-8.08,26,26,0,0,1-2.36-11.64q0-13.61,10-21.59t27.41-8a78.92,78.92,0,0,1,8.68.49q4.33.5,8.09,1.19c2.49.46,4.7,1,6.6,1.48s3.39,1,4.44,1.38l-3.35,15.77a46.51,46.51,0,0,0-9.27-3.25A60.18,60.18,0,0,0,1137,110.7a28,28,0,0,0-13.4,3.06,10.12,10.12,0,0,0-5.72,9.56,13,13,0,0,0,1.28,5.92,13.74,13.74,0,0,0,3.94,4.64,32.46,32.46,0,0,0,6.61,3.84q3.94,1.77,9.46,3.75,7.3,2.76,13,5.42a40.62,40.62,0,0,1,9.76,6.21,24,24,0,0,1,6.21,8.58,31,31,0,0,1,2.17,12.32q0,14.2-10.55,21.5t-30.07,7.29q-13.61,0-21.3-2.26a109.28,109.28,0,0,1-10.45-3.46l3.35-15.77q3.15,1.19,10.06,3.55T1129.67,187.22Z" />
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1083.07,200.43l-40.82,2.17a45.24,45.24,0,0,1-20.32-3.46,31.81,31.81,0,0,1-12.91-9.66,37.28,37.28,0,0,1-6.8-14.89,86,86,0,0,1-2-19.12V97.89h18.34v53.63a88,88,0,0,0,1.28,16.17,27.46,27.46,0,0,0,4.24,10.85,16.86,16.86,0,0,0,7.89,6c3.29,1.25,8.76.92,13.61.57,11.37-.82,14-1.15,19.13-1.52V97.89h18.34Z" />
                                    <g>
                                        <rect class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" x="964.71" y="97.88" width="18.34" height="102.54" />
                                        <circle class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" cx="973.88" cy="57.2" r="11.83" />
                                    </g>
                                </g>
                                <g>
                                    <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M548.46,97.89c-4.34-1.06-29.91-2.17-38.46-2.17a54.18,54.18,0,0,0-20.9,3.84,44.89,44.89,0,0,0-15.68,10.65,45.93,45.93,0,0,0-9.76,16.17A60.9,60.9,0,0,0,460.31,147c0,8.55,1.34,23,3.7,29.19a44.3,44.3,0,0,0,9.67,15.48A37.85,37.85,0,0,0,488,200.83a51,51,0,0,0,17.46,3,46.4,46.4,0,0,0,15.87-2.47,49.18,49.18,0,0,0,9.17-4v4.34q0,14.19-7.2,20.7t-23.56,6.51c-2.5,0-19.84,0-25.33-.12v15.87c5.38,0,22.41,0,24.93,0q24.85,0,37.07-10.65t12.23-34.32Zm-27.85,88.05A41.63,41.63,0,0,1,507.2,188a28.19,28.19,0,0,1-10.35-2,23.8,23.8,0,0,1-8.88-6.12,30.06,30.06,0,0,1-6.11-10.45c-1.51-4.2-2.42-16.37-2.42-22.29q0-16,8-25.63t22.77-9.66a89.19,89.19,0,0,1,12.52.69,59.68,59.68,0,0,1,7.4,1.48l.15,67.05A33,33,0,0,1,520.61,185.94Z" />
                                    <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M448.12,149.16a64.73,64.73,0,0,1-3.55,22.08,50.53,50.53,0,0,1-10,17,44,44,0,0,1-15.28,11,50.55,50.55,0,0,1-38.65,0,43.87,43.87,0,0,1-15.28-11,50.2,50.2,0,0,1-10-17,64.73,64.73,0,0,1-3.55-22.08,65,65,0,0,1,3.55-22,50.18,50.18,0,0,1,10-17.06,43.71,43.71,0,0,1,15.28-10.94,50.43,50.43,0,0,1,38.65,0,43.8,43.8,0,0,1,15.28,10.94,50.51,50.51,0,0,1,10,17.06A65,65,0,0,1,448.12,149.16Zm-19.13,0q0-17.35-7.79-27.51T400,111.49q-13.41,0-21.19,10.16T371,149.16q0,17.35,7.79,27.51T400,186.82q13.41,0,21.2-10.15T429,149.16Z" />
                                    <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M601.31,97.89l40.82-2.17a45.09,45.09,0,0,1,20.31,3.45,31.74,31.74,0,0,1,12.91,9.66,37.49,37.49,0,0,1,6.81,14.89,86.63,86.63,0,0,1,2,19.13v57.58H665.79V146.79a88,88,0,0,0-1.28-16.17,27.46,27.46,0,0,0-4.24-10.85,16.81,16.81,0,0,0-7.89-6c-3.29-1.25-7.6-.9-12.46-.65l-20.27,1v86.29H601.31Z" />
                                    <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M337.92,202.4c-.93-.54-22.59-13.71-22.59-32.46V45.37h18.34V169.94c0,11,15.18,20.38,15.32,20.5Z" />
                                    <g>
                                        <rect class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" x="565.77" y="97.89" width="18.34" height="102.54" />
                                        <circle class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" cx="574.94" cy="57.2" r="11.83" />
                                    </g>
                                </g>
                            </g>
                            <g>
                                <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1187.17,178.21a10.38,10.38,0,1,1-10.38,10.38,10.4,10.4,0,0,1,10.38-10.38m0-1.45A11.83,11.83,0,1,0,1199,188.59a11.82,11.82,0,0,0-11.83-11.83Z" />
                                <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1189.09,189.26c.19.24.43.54.71.92s.58.8.88,1.25.6.94.9,1.44a14.6,14.6,0,0,1,.76,1.45h-1.85c-.23-.44-.49-.89-.76-1.34s-.55-.89-.83-1.3-.55-.8-.82-1.16-.52-.67-.75-.94l-.46,0h-2v4.72h-1.69V182.41a9.94,9.94,0,0,1,1.59-.23c.59,0,1.13-.06,1.61-.06a6.67,6.67,0,0,1,3.88.95,3.31,3.31,0,0,1,1.34,2.86A3.37,3.37,0,0,1,1191,188,3.67,3.67,0,0,1,1189.09,189.26Zm-2.56-5.66c-.72,0-1.27,0-1.65.05v4.55h1.2a12.43,12.43,0,0,0,1.57-.09,3.28,3.28,0,0,0,1.18-.33,1.82,1.82,0,0,0,.74-.69,2.7,2.7,0,0,0,0-2.31,1.93,1.93,0,0,0-.71-.71,3,3,0,0,0-1-.37A7.39,7.39,0,0,0,1186.53,183.6Z" />
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </a>
    </div>

    <div class="lvl1">
        <div>
            <a class="logo" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" width="1200" height="246" viewBox="0 0 1200 246">
                    <defs>
                        <style>
                            .dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf {
                                fill: #008ecf;
                            }

                            .hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb {
                                fill: #09263c;
                            }

                        </style>
                    </defs>
                    <title>LoginRadius Logo</title>
                    <g id="horizontals">
                        <g>
                            <g>
                                <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M126.9,1.18A125.9,125.9,0,0,0,82.29,244.82l10.64-29.64a94.43,94.43,0,1,1,67.93,0l10.64,29.64A125.9,125.9,0,0,0,126.9,1.18Z" />
                                <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M103.57,185.54l10.65-29.67a31.47,31.47,0,1,1,25.36,0l10.65,29.67a63,63,0,1,0-46.66,0Z" />
                            </g>
                            <g>
                                <g>
                                    <g>
                                        <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M739.19,95.72c1.57,0,3.38.09,5.42.29s4,.46,6,.79,3.78.66,5.43,1a33.88,33.88,0,0,1,3.64.88l-3.15,16a62.15,62.15,0,0,0-7.2-1.88,63.32,63.32,0,0,0-12.91-1.08,48.24,48.24,0,0,0-10.16,1.08c-3.35.73-5.56,1.22-6.61,1.48v86.18H701.33V102.22a125.18,125.18,0,0,1,16.17-4.43A103.81,103.81,0,0,1,739.19,95.72Z" />
                                        <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M803.49,95.32q11,0,18.63,2.86a30.21,30.21,0,0,1,12.23,8.09,30.61,30.61,0,0,1,6.6,12.42,60.2,60.2,0,0,1,2,15.88v64.08c-1.57.27-3.78.63-6.6,1.09s-6,.88-9.57,1.28-7.39.75-11.53,1.08-8.25.5-12.33.5a67.15,67.15,0,0,1-16-1.78,35.76,35.76,0,0,1-12.62-5.62A25.64,25.64,0,0,1,766,185.05a35.6,35.6,0,0,1-3-15.19,29.24,29.24,0,0,1,3.45-14.59,27.5,27.5,0,0,1,9.37-9.86,43.18,43.18,0,0,1,13.8-5.52,75.48,75.48,0,0,1,16.57-1.78,56.66,56.66,0,0,1,5.71.3q3,.3,5.62.79c1.78.33,3.32.62,4.64.89s2.23.46,2.76.59v-5.13a41.09,41.09,0,0,0-1-9,20.54,20.54,0,0,0-3.55-7.89,18,18,0,0,0-7-5.52,27.41,27.41,0,0,0-11.53-2.07A87.13,87.13,0,0,0,786,112.38,55.94,55.94,0,0,0,775.88,115l-2.17-15.18a56.9,56.9,0,0,1,11.83-3.06A101.79,101.79,0,0,1,803.49,95.32Zm1.57,91.9q6.51,0,11.54-.3a51.45,51.45,0,0,0,8.38-1.08V155.27a23.79,23.79,0,0,0-6.41-1.68,71.33,71.33,0,0,0-10.75-.69,68.41,68.41,0,0,0-8.77.6,26.86,26.86,0,0,0-8.48,2.46,18.22,18.22,0,0,0-6.41,5.13,13.45,13.45,0,0,0-2.56,8.57q0,9.87,6.31,13.71T805.06,187.22Z" />
                                        <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M929.17,45.37h18.34V200.43c-4.21,1.18-30.31,2.36-38.85,2.36a57.62,57.62,0,0,1-21.3-3.74,45.69,45.69,0,0,1-16.17-10.65,47,47,0,0,1-10.35-16.86,64.54,64.54,0,0,1-3.65-22.38,73,73,0,0,1,3.06-21.7,48.53,48.53,0,0,1,9-16.95,40.75,40.75,0,0,1,14.49-11.05,46.49,46.49,0,0,1,19.63-3.94A47.54,47.54,0,0,1,919,97.89a49.53,49.53,0,0,1,10.16,4.53Zm0,74.21a38,38,0,0,0-9.66-5.33,36.54,36.54,0,0,0-13.81-2.56,29.58,29.58,0,0,0-13.51,2.86,24.85,24.85,0,0,0-9.17,7.89,33.38,33.38,0,0,0-5.12,11.93,65.77,65.77,0,0,0-1.58,14.79q0,17.94,8.87,27.7c5.92,6.51,13.95,8.77,23.81,8.77,5,0,18.33-.65,20.17-1.17Z" />
                                        <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1129.67,187.22q11.24,0,16.66-3t5.42-9.47a12.58,12.58,0,0,0-5.32-10.65q-5.33-3.93-17.55-8.87-5.91-2.37-11.34-4.83a39.44,39.44,0,0,1-9.37-5.82,25.4,25.4,0,0,1-6.31-8.08,26,26,0,0,1-2.36-11.64q0-13.61,10-21.59t27.41-8a78.92,78.92,0,0,1,8.68.49q4.33.5,8.09,1.19c2.49.46,4.7,1,6.6,1.48s3.39,1,4.44,1.38l-3.35,15.77a46.51,46.51,0,0,0-9.27-3.25A60.18,60.18,0,0,0,1137,110.7a28,28,0,0,0-13.4,3.06,10.12,10.12,0,0,0-5.72,9.56,13,13,0,0,0,1.28,5.92,13.74,13.74,0,0,0,3.94,4.64,32.46,32.46,0,0,0,6.61,3.84q3.94,1.77,9.46,3.75,7.3,2.76,13,5.42a40.62,40.62,0,0,1,9.76,6.21,24,24,0,0,1,6.21,8.58,31,31,0,0,1,2.17,12.32q0,14.2-10.55,21.5t-30.07,7.29q-13.61,0-21.3-2.26a109.28,109.28,0,0,1-10.45-3.46l3.35-15.77q3.15,1.19,10.06,3.55T1129.67,187.22Z" />
                                        <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1083.07,200.43l-40.82,2.17a45.24,45.24,0,0,1-20.32-3.46,31.81,31.81,0,0,1-12.91-9.66,37.28,37.28,0,0,1-6.8-14.89,86,86,0,0,1-2-19.12V97.89h18.34v53.63a88,88,0,0,0,1.28,16.17,27.46,27.46,0,0,0,4.24,10.85,16.86,16.86,0,0,0,7.89,6c3.29,1.25,8.76.92,13.61.57,11.37-.82,14-1.15,19.13-1.52V97.89h18.34Z" />
                                        <g>
                                            <rect class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" x="964.71" y="97.88" width="18.34" height="102.54" />
                                            <circle class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" cx="973.88" cy="57.2" r="11.83" />
                                        </g>
                                    </g>
                                    <g>
                                        <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M548.46,97.89c-4.34-1.06-29.91-2.17-38.46-2.17a54.18,54.18,0,0,0-20.9,3.84,44.89,44.89,0,0,0-15.68,10.65,45.93,45.93,0,0,0-9.76,16.17A60.9,60.9,0,0,0,460.31,147c0,8.55,1.34,23,3.7,29.19a44.3,44.3,0,0,0,9.67,15.48A37.85,37.85,0,0,0,488,200.83a51,51,0,0,0,17.46,3,46.4,46.4,0,0,0,15.87-2.47,49.18,49.18,0,0,0,9.17-4v4.34q0,14.19-7.2,20.7t-23.56,6.51c-2.5,0-19.84,0-25.33-.12v15.87c5.38,0,22.41,0,24.93,0q24.85,0,37.07-10.65t12.23-34.32Zm-27.85,88.05A41.63,41.63,0,0,1,507.2,188a28.19,28.19,0,0,1-10.35-2,23.8,23.8,0,0,1-8.88-6.12,30.06,30.06,0,0,1-6.11-10.45c-1.51-4.2-2.42-16.37-2.42-22.29q0-16,8-25.63t22.77-9.66a89.19,89.19,0,0,1,12.52.69,59.68,59.68,0,0,1,7.4,1.48l.15,67.05A33,33,0,0,1,520.61,185.94Z" />
                                        <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M448.12,149.16a64.73,64.73,0,0,1-3.55,22.08,50.53,50.53,0,0,1-10,17,44,44,0,0,1-15.28,11,50.55,50.55,0,0,1-38.65,0,43.87,43.87,0,0,1-15.28-11,50.2,50.2,0,0,1-10-17,64.73,64.73,0,0,1-3.55-22.08,65,65,0,0,1,3.55-22,50.18,50.18,0,0,1,10-17.06,43.71,43.71,0,0,1,15.28-10.94,50.43,50.43,0,0,1,38.65,0,43.8,43.8,0,0,1,15.28,10.94,50.51,50.51,0,0,1,10,17.06A65,65,0,0,1,448.12,149.16Zm-19.13,0q0-17.35-7.79-27.51T400,111.49q-13.41,0-21.19,10.16T371,149.16q0,17.35,7.79,27.51T400,186.82q13.41,0,21.2-10.15T429,149.16Z" />
                                        <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M601.31,97.89l40.82-2.17a45.09,45.09,0,0,1,20.31,3.45,31.74,31.74,0,0,1,12.91,9.66,37.49,37.49,0,0,1,6.81,14.89,86.63,86.63,0,0,1,2,19.13v57.58H665.79V146.79a88,88,0,0,0-1.28-16.17,27.46,27.46,0,0,0-4.24-10.85,16.81,16.81,0,0,0-7.89-6c-3.29-1.25-7.6-.9-12.46-.65l-20.27,1v86.29H601.31Z" />
                                        <path class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" d="M337.92,202.4c-.93-.54-22.59-13.71-22.59-32.46V45.37h18.34V169.94c0,11,15.18,20.38,15.32,20.5Z" />
                                        <g>
                                            <rect class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" x="565.77" y="97.89" width="18.34" height="102.54" />
                                            <circle class="dxcfhgjvhbjkgjvfcghdxghcgvhbjnbjgvjfgchdf" cx="574.94" cy="57.2" r="11.83" />
                                        </g>
                                    </g>
                                </g>
                                <g>
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1187.17,178.21a10.38,10.38,0,1,1-10.38,10.38,10.4,10.4,0,0,1,10.38-10.38m0-1.45A11.83,11.83,0,1,0,1199,188.59a11.82,11.82,0,0,0-11.83-11.83Z" />
                                    <path class="hcdftvjgbhnjihkgjvyfchtdtfvjgbkhnbgjvyfhtcvgyb" d="M1189.09,189.26c.19.24.43.54.71.92s.58.8.88,1.25.6.94.9,1.44a14.6,14.6,0,0,1,.76,1.45h-1.85c-.23-.44-.49-.89-.76-1.34s-.55-.89-.83-1.3-.55-.8-.82-1.16-.52-.67-.75-.94l-.46,0h-2v4.72h-1.69V182.41a9.94,9.94,0,0,1,1.59-.23c.59,0,1.13-.06,1.61-.06a6.67,6.67,0,0,1,3.88.95,3.31,3.31,0,0,1,1.34,2.86A3.37,3.37,0,0,1,1191,188,3.67,3.67,0,0,1,1189.09,189.26Zm-2.56-5.66c-.72,0-1.27,0-1.65.05v4.55h1.2a12.43,12.43,0,0,0,1.57-.09,3.28,3.28,0,0,0,1.18-.33,1.82,1.82,0,0,0,.74-.69,2.7,2.7,0,0,0,0-2.31,1.93,1.93,0,0,0-.71-.71,3,3,0,0,0-1-.37A7.39,7.39,0,0,0,1186.53,183.6Z" />
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </a>

            <div class="main-links">
                <a href="/ws-key-benefits">Why LoginRadius?</a>
                <a href="/ws-authentication">Products</a>
                <a href="/resources-2">Resources</a>
                <a href="/pricing3-platform">Pricing</a>
            </div>
        </div>








        <div id="rightSide">
            <a class="btn-primary" href="https://accounts.loginradius.com/auth.aspx?action=register">Free Sign Up</a>

            <a class="btn-login" href="https://accounts.loginradius.com/auth.aspx?plan=developer">Login</a>

            <a class="btn-search mega-menu-search" tabindex="0">
                <p class="search-label-mobile">Search</p>

                <div class="mega-menu-search-dark-icon"></div>

                <form tabindex="0" class="search-form " itemprop="potentialAction" itemscope="" itemtype="https://schema.org/SearchAction" method="get" action="https://www.loginradius.com/" role="search" data-children-count="1">


                    <!--                    searchInputClose-->
                    <label tabindex="-1" class="search-form-label screen-reader-text" for="searchform-1">Search this website</label>
                    <input tabindex="-1" class="search-form-input " type="search" itemprop="query-input" name="s" id="searchform-1" placeholder="Search" autocomplete="off">
                    <!--                    searchInputWtoB-->

                    <input tabindex="0" class="search-form-submit" type="submit" value="Search">
                    <meta tabindex="-1" itemprop="target" content="https://www.loginradius.com/?s={s}">
                </form>
            </a>


        </div>



    </div>


    <?php
    $wsMenuLvl1 = get_post_meta(get_the_ID(), 'ws_menu_lvl1', true);
    $wsMenuLvl2 = get_post_meta(get_the_ID(), 'ws_menu_lvl2', true);
    
    $wsMenuLvl1 = strtolower($wsMenuLvl1);
    $wsMenuLvl2 = strtolower($wsMenuLvl2);
    
    $wsMenuLvl1 = str_replace(" ", "", $wsMenuLvl1);
    $wsMenuLvl2 = str_replace(" ", "", $wsMenuLvl2);
    
    $wsMenuLvl1 = str_replace("-", "", $wsMenuLvl1);
    $wsMenuLvl2 = str_replace("-", "", $wsMenuLvl2);
    
    $wsMenuLvl1 = str_replace("_", "", $wsMenuLvl1);
    $wsMenuLvl2 = str_replace("_", "", $wsMenuLvl2);
    
    
    if($wsMenuLvl1 == "whyloginradius") {
    ?>


    <div class="lvl2 active" id="whyloginradius">

        <div class="shadow"></div>

        <div class="items dragscroll">

            <div>
                <a href="/ws-key-benefits" <?php if($wsMenuLvl2 == "1") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>

                    <svg id="f33c020a-b9f6-4b35-95d0-88a33ab07913" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .afde2070-f876-4708-9640-e5f3b408a772 {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="b9485e30-fa5d-43c9-ae27-45be80de4a4a" data-name="Social-Medias-Rewards-Rating / Love-It / love-it-ribbon-1">
                            <g id="b2a19b32-a10a-4cbe-907d-e3c51d1bfaf1" data-name="Group 54">
                                <g id="ff5a5634-3039-49a3-9271-7a5fa64fc37c" data-name="love-it-ribbon-1">
                                    <path id="edd6f7bd-d1ec-4b38-ae26-48a6da186471" data-name="Shape 228" class="afde2070-f876-4708-9640-e5f3b408a772" d="M37.83,43.58a2.08,2.08,0,0,0,2.6,0c2.55-2.07,8-6.92,8-10.74,0-5.78-7.6-7.59-9.28-.58-1.69-7-9.28-5.19-9.28.58C29.85,36.66,35.28,41.51,37.83,43.58Z" />
                                    <path id="b5d4acbe-5ac6-4f23-9d85-3b743cac54c1" data-name="Shape 229" class="afde2070-f876-4708-9640-e5f3b408a772" d="M22.94,44.32,15.54,57c-.29.49-.06.81.5.72L23,56.54l2.4,6.6c.2.54.59.57.88.09l6.32-10.62" />
                                    <path id="f14a20a7-8b87-40e9-adaa-04c6c189258f" data-name="Shape 230" class="afde2070-f876-4708-9640-e5f3b408a772" d="M55.32,44.32,62.72,57c.28.49.06.81-.5.72l-6.93-1.18-2.4,6.6c-.19.54-.59.57-.88.09L45.69,52.61" />
                                    <path id="a3533e1e-fc52-4c64-bfbc-e5579fa7c3fa" data-name="Oval 20" class="afde2070-f876-4708-9640-e5f3b408a772" d="M39.13,53.8A18.56,18.56,0,1,0,20.57,35.25,18.55,18.55,0,0,0,39.13,53.8Z" />
                                </g>
                            </g>
                        </g>
                    </svg>



                    <p>Key Benefits</p>
                </a>

                <a href="/customers2" <?php if($wsMenuLvl2 == "2") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="b2dcac5e-8cfc-45f0-bc25-f2eb573ad0ac" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .a899a351-facd-44f9-9437-a4bd858d8b8c {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="a7d58aee-614b-471a-b01d-3177be28ce8e" data-name="Social-Medias-Rewards-Rating / Rating / rating-star-give">
                            <g id="b6d9edc5-19d3-4f68-81fb-9cea0162637c" data-name="Group 77">
                                <g id="bcb689a7-b459-4bb5-adb1-01b8d5f0e997" data-name="rating-star-give">
                                    <path id="e118353f-97fc-44d5-955c-7a9807d10fdd" data-name="Shape 288" class="a899a351-facd-44f9-9437-a4bd858d8b8c" d="M35.81,42.65,38,35.87l-9.4-8.31a1.13,1.13,0,0,1,.74-2H40.52L44.47,15a1.13,1.13,0,0,1,2.12,0l3.95,10.56H61.71a1.13,1.13,0,0,1,.75,2L53,35.9,57,47.79a1.12,1.12,0,0,1-.4,1.26,1.14,1.14,0,0,1-1.33,0l-9.69-7-4.77,3.52" />
                                    <path id="a6e88779-63b3-4e7e-96cf-a2cfd4b13510" data-name="Shape 289" class="a899a351-facd-44f9-9437-a4bd858d8b8c" d="M15.41,64V41.39" />
                                    <path id="ed1cbf2e-17bb-406f-b8e4-fef14c348af2" data-name="Shape 290" class="a899a351-facd-44f9-9437-a4bd858d8b8c" d="M15.41,45.9H31.22A6.78,6.78,0,0,1,38,52.68H51.55a6.78,6.78,0,0,1,6.78,6.77H15.41Z" />
                                    <path id="fe9392e4-c992-4ce1-be5e-adb2d37bb6f5" data-name="Shape 291" class="a899a351-facd-44f9-9437-a4bd858d8b8c" d="M38,52.68H31.22" />
                                </g>
                            </g>
                        </g>
                    </svg>


                    <p>Customers</p>
                </a>

            </div>
        </div>
    </div>


    <?php   
    } else if($wsMenuLvl1 == "products") {
    ?>


    <div class="lvl2 active" id="products">

        <div class="shadow"></div>

        <div class="items dragscroll">

            <div>
                <a href="/ws-authentication" <?php if($wsMenuLvl2 == "1") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>

                    <svg id="bb84ca0f-2c87-43be-ba4f-ddf425e1e413" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .be4c5420-b22d-4eec-b930-e7c7a9ac6e8a {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="f74ec2c3-16d5-4b0b-bcbc-da646f549134" data-name="Programming-Apps-Websites / Bug/Security / shield-check-1">
                            <g id="bd474aa5-5218-44ad-b821-1945fefedf48" data-name="Group 69">
                                <g id="a3c3de35-47c9-46d8-abac-6a655d87fa5c" data-name="shield-check-1">
                                    <path id="a5a4e63a-b3bc-4b9b-ac03-9b8abab9612b" data-name="Shape 491" class="be4c5420-b22d-4eec-b930-e7c7a9ac6e8a" d="M50.57,36.69A11.45,11.45,0,1,1,39.13,25.25" />
                                    <path id="ea6df4a3-8566-4d5f-9f94-d7d361ea20a4" data-name="Shape 492" class="be4c5420-b22d-4eec-b930-e7c7a9ac6e8a" d="M34.55,35.55l3.71,2.78a1.13,1.13,0,0,0,1.56-.18l10.75-12.9" />
                                    <path id="fe167ec9-6bef-4655-8183-d4cad540761a" data-name="Shape 493" class="be4c5420-b22d-4eec-b930-e7c7a9ac6e8a" d="M15.09,21.3V38.78A28.19,28.19,0,0,0,33.91,65l2.56.95a7.7,7.7,0,0,0,5.31,0L44.35,65A28.2,28.2,0,0,0,63.17,38.78V21.3A3.52,3.52,0,0,0,61,18.08a55.45,55.45,0,0,0-21.9-4.28,55.38,55.38,0,0,0-21.9,4.28A3.49,3.49,0,0,0,15.09,21.3Z" />
                                </g>
                            </g>
                        </g>
                    </svg>



                    <p>Authentication</p>
                </a>

                <a href="/hsbh" <?php if($wsMenuLvl2 == "2") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="b77f753b-23e4-40a6-99ba-eeb76dce9f05" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .b91c059c-dad7-4de4-896c-38b6b637160e {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g>
                            <circle class="b91c059c-dad7-4de4-896c-38b6b637160e" cx="39.13" cy="20.82" r="7.83" />
                            <rect class="b91c059c-dad7-4de4-896c-38b6b637160e" x="19.71" y="33.68" width="38.83" height="7.91" rx="3.95" />
                            <rect class="b91c059c-dad7-4de4-896c-38b6b637160e" x="19.71" y="45.52" width="38.83" height="7.91" rx="3.95" />
                            <rect class="b91c059c-dad7-4de4-896c-38b6b637160e" x="19.71" y="57.36" width="38.83" height="7.91" rx="3.95" />
                        </g>
                    </svg>


                    <p>Social Login</p>
                </a>

                <a href="/hsbh" <?php if($wsMenuLvl2 == "3") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="bf4959e3-7615-4a9b-925b-db380fe56a34" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .be9750bf-4fce-438d-afb4-359d60d1e2cd {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <path id="b430e3d8-1088-46a0-aad2-a70c343a62ce" data-name="Shape 164" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M48.4,38.1h6.18" />
                        <path id="a908a5de-f316-4020-a43b-a8b5dcc424c2" data-name="Oval 34" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M58.7,42.22a4.12,4.12,0,1,0-4.12-4.12A4.12,4.12,0,0,0,58.7,42.22Z" />
                        <path id="fd391941-8fcc-46e9-b1e9-edc63a047cf2" data-name="Shape 165" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M29.86,38.1H23.68" />
                        <path id="b55d58de-0d59-4570-9b3e-d64ab63aca0b" data-name="Oval 35" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M19.56,42.22a4.12,4.12,0,1,0-4.13-4.12A4.13,4.13,0,0,0,19.56,42.22Z" />
                        <path id="ee608831-3804-464d-8a7e-e95a310970c0" data-name="Shape 166" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M29.88,48.4a9.27,9.27,0,0,1,18.54,0Z" />
                        <path id="a866f404-6ce5-4fca-9cc3-66018464c229" data-name="Oval 36" class="be9750bf-4fce-438d-afb4-359d60d1e2cd" d="M39.15,35.58a5.67,5.67,0,1,0-5.67-5.66A5.65,5.65,0,0,0,39.15,35.58Z" />
                    </svg>



                    <p>Developer</p>
                </a>

                <a href="/hsbh" <?php if($wsMenuLvl2 == "4") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="f9121e05-c456-4d71-8caf-a96a627b2f0e" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .af4fe9a4-36f1-4816-9cb8-45ccca1e32e6 {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="bf5d4355-c49b-46a9-a397-97f9ae01dcb2" data-name="Business-Products / Business / user-network">
                            <g id="f812ac75-de96-46e2-a65e-5380db14d6f9" data-name="Group 28">
                                <g id="f77d3a4d-e628-4fa4-b09d-23b864843083" data-name="user-network">
                                    <path id="bd3756df-67cf-4dd2-9385-d9e127330e00" data-name="Shape 160" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M49.19,28.87l6.41-6.36" />
                                    <path id="f56d3086-4ffa-4d3a-9806-dc43ff0c3090" data-name="Oval 30" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M58.46,23.68a4.13,4.13,0,1,0-4.12-4.12A4.12,4.12,0,0,0,58.46,23.68Z" />
                                    <path id="b2e8b37c-70a4-4532-b8b3-973e7cf598ed" data-name="Shape 161" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M51.17,51.45l4.36,4.38" />
                                    <path id="a5788b00-f0a7-4f51-990c-23bb36982606" data-name="Oval 31" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M58.48,62.82a4.12,4.12,0,1,0-4.12-4.12A4.11,4.11,0,0,0,58.48,62.82Z" />
                                    <path id="a5073d60-f07d-4e4a-bc85-f37b4fa5cbb9" data-name="Shape 162" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M28.59,28.87l-6.4-6.36" />
                                    <path id="e9a83941-8ed2-4787-9982-d5980911c07a" data-name="Oval 32" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M19.32,23.68a4.13,4.13,0,1,0-4.12-4.12A4.13,4.13,0,0,0,19.32,23.68Z" />
                                    <path id="f1b5afa2-b282-4af3-b0d1-eddaa4e1d5f3" data-name="Shape 163" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M26.61,51.45l-4.36,4.38" />
                                    <path id="bcf92d87-a9c2-46cc-abdd-0e4b4ed52364" data-name="Oval 33" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M19.3,62.82a4.12,4.12,0,1,0-4.12-4.12A4.12,4.12,0,0,0,19.3,62.82Z" />
                                    <path id="accbfad7-87b9-4579-b5d9-d5e8ff5b8566" data-name="Shape 166" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M29.64,48.4a9.27,9.27,0,0,1,18.54,0Z" />
                                    <path id="eb23d804-ff37-4a4a-8d1b-02fc3fc2cff2" data-name="Oval 36" class="af4fe9a4-36f1-4816-9cb8-45ccca1e32e6" d="M38.91,35.58a5.67,5.67,0,1,0-5.66-5.66A5.66,5.66,0,0,0,38.91,35.58Z" />
                                </g>
                            </g>
                        </g>
                    </svg>


                    <p>Developer Pro</p>
                </a>

                <a href="/hsbh" <?php if($wsMenuLvl2 == "5") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="bb8c56ef-9163-49f6-a234-402aeec7559f" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .e4c5d73c-bcc9-4343-8a64-de7798efe2eb {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="e980077f-939d-41d8-aae0-8cb3162a3827" data-name="Business-Products / Business / user-network">
                            <g id="f224ed4c-bf47-42ac-9f3a-2a094268bab6" data-name="Group 28">
                                <g id="a2a6b94c-d024-41be-8f70-c4afa8dbf5b6" data-name="user-network">
                                    <path id="a991034d-8e17-492e-9e45-e3e594e07c17" data-name="Shape 160" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M49.43,28.87l6.4-6.36" />
                                    <path id="b41d834f-0e22-4f78-a703-fac755704eaf" data-name="Oval 30" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M58.7,23.68a4.13,4.13,0,1,0-4.12-4.12A4.12,4.12,0,0,0,58.7,23.68Z" />
                                    <path id="f80126c4-ba9e-4aeb-95d2-c81482f1e2ee" data-name="Shape 161" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M51.41,51.45l4.36,4.38" />
                                    <path id="fb66c35b-2981-43a9-be2e-273604be5c40" data-name="Oval 31" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M58.72,62.82A4.12,4.12,0,1,0,54.6,58.7,4.11,4.11,0,0,0,58.72,62.82Z" />
                                    <path id="ae9f9902-7f78-4ec3-a15a-6bd3ef61f643" data-name="Shape 162" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M28.83,28.87l-6.4-6.36" />
                                    <path id="fac2ee82-c539-44e0-8874-02df09d99144" data-name="Oval 32" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M19.56,23.68a4.13,4.13,0,1,0-4.13-4.12A4.13,4.13,0,0,0,19.56,23.68Z" />
                                    <path id="b5ad86b4-0aad-4172-9e11-76f9d1df15db" data-name="Shape 163" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M26.85,51.45l-4.36,4.38" />
                                    <path id="bc09855b-e053-43b0-a0cf-7ba4ab3dea03" data-name="Oval 33" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M19.53,62.82a4.12,4.12,0,1,0-4.12-4.12A4.12,4.12,0,0,0,19.53,62.82Z" />
                                    <path id="b35ca483-eef4-432f-a2b5-9e8b0783fa9d" data-name="Shape 164" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M48.4,38.1h6.18" />
                                    <path id="ab6d6ef2-bb66-46e6-8bb7-f22a1de88f3f" data-name="Oval 34" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M58.7,42.22a4.12,4.12,0,1,0-4.12-4.12A4.12,4.12,0,0,0,58.7,42.22Z" />
                                    <path id="b5c1cb47-8aba-47ab-abf2-cbb05db4b5f0" data-name="Shape 165" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M29.86,38.1H23.68" />
                                    <path id="e424d5ad-7db1-44f6-9810-54275850702a" data-name="Oval 35" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M19.56,42.22a4.12,4.12,0,1,0-4.13-4.12A4.13,4.13,0,0,0,19.56,42.22Z" />
                                    <path id="f8f60189-12c7-4367-b006-8c191dd4ef37" data-name="Shape 166" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M29.88,48.4a9.27,9.27,0,0,1,18.54,0Z" />
                                    <path id="b4e7ded7-4c8e-479e-be9c-60fa96cabf3f" data-name="Oval 36" class="e4c5d73c-bcc9-4343-8a64-de7798efe2eb" d="M39.15,35.58a5.67,5.67,0,1,0-5.67-5.66A5.65,5.65,0,0,0,39.15,35.58Z" />
                                </g>
                            </g>
                        </g>
                    </svg>


                    <p>Enterprise</p>
                </a>
            </div>
        </div>
    </div>

    <?php   
    } else if($wsMenuLvl1 == "resources") {
    ?>

    <div class="lvl2 active" id="resources">

        <div class="shadow"></div>

        <div class="items dragscroll">

            <div>
                <a href="/resources-2" <?php if($wsMenuLvl2 == "1") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>

                    <svg id="bd6d2da7-9991-4dc6-b49c-bd4aa986912c" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .ec30e88b-8f17-4702-94eb-7dfa45e74d00 {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="bb32067c-c570-45ff-bed5-d5634a2f6e92" data-name="Content / Newspapers / newspaper-read-man">
                            <g id="b91ed49e-e41e-4adb-aafd-a527bfad4115" data-name="Group 115">
                                <g id="a57bdb33-5cdd-48cf-851f-ce79a055975c" data-name="newspaper-read-man">
                                    <path id="f55a50fc-9730-4f39-802d-dde4333dfb17" data-name="Shape 687" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M48.12,27.81c-8,0-8-3-9-4-1,1-1,4-9,4" />
                                    <path id="a1c46d43-c07a-4dc7-9302-446fd00d062e" data-name="Shape 688" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M51.07,32.26l1-7.43s1-10-13-10-13,10-13,10l1.07,7.38" />
                                    <path id="e5035208-cacd-41da-8950-47a63d0bd5bc" data-name="Shape 689" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M33.13,32.82c0-1.1.9-1,2-1s2-.1,2,1" />
                                    <path id="efc29fb0-f602-4eef-87b8-141bdde9592f" data-name="Shape 690" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M41.13,32.82c0-1.1.9-1,2-1s2-.1,2,1" />
                                    <path id="aae2b2b4-7bd7-4609-ab28-253ba22bffd8" data-name="Shape 691" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M17.77,49.68v8.39a2.25,2.25,0,0,0,1.68,2.17l19.68,5.18V40.69L20.69,34.87A2.25,2.25,0,0,0,17.77,37v3.68" />
                                    <path id="b81f345a-6da6-4329-a05e-02a223912a13" data-name="Shape 692" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M39.13,40.69l18.43-5.82A2.25,2.25,0,0,1,60.49,37v3.68" />
                                    <path id="f7ae1dac-9f79-4713-8479-b53791a469ad" data-name="Shape 693" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M60.49,49.68v8.39a2.25,2.25,0,0,1-1.68,2.17L39.13,65.42" />
                                    <path id="be01d026-c5ab-4deb-a9c9-1494719006dc" data-name="Shape 694" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M13.27,45.19c0,3.37,3.15,4.49,5.62,4.49A1.13,1.13,0,0,0,20,48.56V41.81a1.13,1.13,0,0,0-1.13-1.12C16.42,40.69,13.27,41.81,13.27,45.19Z" />
                                    <path id="abfb008d-61b4-46cf-ba99-e2add9f41559" data-name="Shape 695" class="ec30e88b-8f17-4702-94eb-7dfa45e74d00" d="M65,45.19c0,3.37-3.13,4.49-5.62,4.49a1.12,1.12,0,0,1-1.12-1.12V41.81a1.12,1.12,0,0,1,1.12-1.12C61.85,40.69,65,41.81,65,45.19Z" />
                                </g>
                            </g>
                        </g>
                    </svg>



                    <p>Resources</p>
                </a>

                <a href="/ws-blogs" <?php if($wsMenuLvl2 == "2") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="a9163bc8-6b71-44e6-82d8-65bd69c827c5" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .ff622766-1539-4f3e-9f80-2a34bea4be04 {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="b881cbc3-b276-493a-9f62-95fc9e7b1c65" data-name="Content / Content-Creation / content-pencil-write">
                            <g id="bb773342-0a29-4d01-8640-f9a73d205161" data-name="Group 20">
                                <g id="e570a42b-c350-42ec-8ea1-244692a3b0ae" data-name="content-pencil-write">
                                    <path id="ee850642-e21b-4f6a-8f5d-9fd02eb4b91a" data-name="Shape 101" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M54,32.45V20.65a4.3,4.3,0,0,0-4.29-4.29H23.94a4.29,4.29,0,0,0-4.29,4.29V55a4.29,4.29,0,0,0,4.29,4.29H41.1l6.44,6.44V59.26h2.15a4.29,4.29,0,0,0,3.69-2.11" />
                                    <path id="fb184f98-561a-4ce6-9c7f-13e5416b7621" data-name="Shape 103" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M23.94,22.79h8.58" />
                                    <path id="b70d8562-8202-48ca-83e1-5e499d0d9530" data-name="Shape 104" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M28.23,32.45V22.79" />
                                    <path id="adacf282-d6db-4d59-9f34-05847dbcb6ef" data-name="Shape 105" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M45.87,30.78l-9-2.58,2.57,9,15.4,15.4a4.55,4.55,0,0,0,6.44-6.43Z" />
                                    <path id="f18e2898-dbf4-42cf-8cf1-ed5a41bb98d2" data-name="Shape 106" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M52.9,50.68l6.44-6.44" />
                                    <path id="ef91bcd9-fa18-49f1-8219-ee9be3c531a8" data-name="Shape 107" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M39.43,37.21l6.44-6.43" />
                                    <path id="eb63855c-6db8-402e-8f7d-6dc3959b0972" data-name="Shape 108" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M39,22.79h8.58" />
                                    <path id="bc1ff414-5182-48c8-b4c9-722a00f0e0bf" data-name="Shape 109" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M26.09,40h8.58" />
                                    <path id="a75bbdd2-3072-43cd-8309-4a7193a70096" data-name="Shape 110" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M26.09,46.39H40" />
                                    <path id="b002d977-7dc5-422c-a5f5-7e9173f47e35" data-name="Shape 111" class="ff622766-1539-4f3e-9f80-2a34bea4be04" d="M26.09,52.82H45.4" />
                                </g>
                            </g>
                        </g>
                    </svg>



                    <p>Blogs</p>
                </a>

                <a href="/press-2" <?php if($wsMenuLvl2 == "3") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg id="b8599531-85e4-4925-add3-c9284b36a9cc" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.26 78.26">
                        <defs>
                            <style>
                                .f108c837-194b-47db-9701-d5b5d480e8e6 {
                                    fill: none;
                                    stroke: #008ecf;
                                    stroke-linecap: round;
                                    stroke-linejoin: round;
                                    stroke-width: 2px;
                                }

                            </style>
                        </defs>
                        <g id="f02cd8ce-85f1-490b-994e-e6abbfca9e74" data-name="Content / Newspapers / newspaper-fold">
                            <g id="b76f42f5-8949-46b0-946b-b0ed32324b7b" data-name="Group 113">
                                <g id="a1095846-ad47-4144-8861-909020b5cd9b" data-name="newspaper-fold">
                                    <path id="b68c3750-fba4-47ec-9189-dba3143fd490" data-name="Shape 676" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M34.78,38.89A1.07,1.07,0,0,1,33.71,40H23a1.07,1.07,0,0,1-1.07-1.07V28.19A1.07,1.07,0,0,1,23,27.12h10.7a1.07,1.07,0,0,1,1.07,1.07Z" />
                                    <path id="e442e63e-12e8-4eb5-9d06-fe6fafb530ca" data-name="Shape 677" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M39.06,27.12h6.42" />
                                    <path id="a0130c34-d66d-42d8-9e83-cd64d6acead7" data-name="Shape 678" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M39.06,33.54h6.42" />
                                    <path id="b3443a03-7af9-4321-942b-d19cd90712a1" data-name="Shape 679" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M39.06,40h6.42" />
                                    <path id="b276914b-ecd0-45b0-8d9d-1083dc4b980d" data-name="Shape 680" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M21.94,46.37H45.48" />
                                    <path id="adb5602d-8f1d-4677-9808-ec3ce6f91284" data-name="Shape 681" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M21.94,52.79H45.48" />
                                    <path id="bc965153-3ef1-4c48-ba19-a15e3416b385" data-name="Shape 682" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M58.31,63.49H21.94a6.41,6.41,0,0,1-6.41-6.42V20.7a2.14,2.14,0,0,1,2.14-2.14H49.75a2.14,2.14,0,0,1,2.14,2.14V57.07a6.42,6.42,0,0,0,12.84,0V25" />
                                    <path id="a3191811-e4a9-4382-a661-bb5bb1eed45d" data-name="Shape 683" class="f108c837-194b-47db-9701-d5b5d480e8e6" d="M58.31,25V58.14" />
                                </g>
                            </g>
                        </g>
                    </svg>



                    <p>Press</p>
                </a>

            </div>
        </div>
    </div>

    <?php   
    } else if($wsMenuLvl1 == "pricing") {
    ?>

    <div class="lvl2 active" id="pricing">

        <div class="shadow"></div>

        <div class="items dragscroll">

            <div>
                <a href="/pricing3-platform/" <?php if($wsMenuLvl2 == "1") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>
                    <svg version="1.1" id="sdvdfgvdfv" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 78.3 78.3" style="enable-background:new 0 0 78.3 78.3;" xml:space="preserve">
                        <style type="text/css">
                            .scercercwcwecw {
                                fill: none;
                                stroke: #008ECF;
                                stroke-width: 2;
                                stroke-linecap: round;
                                stroke-linejoin: round;
                                stroke-miterlimit: 10;
                            }

                        </style>
                        <g id="Business-Products__x2F__Business__x2F__user-network_34_">
                            <g id="Group_28_34_">
                                <g id="user-network_34_">
                                    <path id="Shape_160_15_" class="scercercwcwecw" d="M49.4,28.9l6.4-6.4" />
                                    <path id="Oval_30_15_" class="scercercwcwecw" d="M58.7,23.7c2.3,0,4.1-1.8,4.1-4.1s-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				S56.4,23.7,58.7,23.7z" />
                                    <path id="Shape_161_15_" class="scercercwcwecw" d="M51.4,51.4l4.4,4.4" />
                                    <path id="Oval_31_15_" class="scercercwcwecw" d="M58.7,62.8c2.3,0,4.1-1.8,4.1-4.1s-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				S56.4,62.8,58.7,62.8z" />
                                    <path id="Shape_162_15_" class="scercercwcwecw" d="M28.8,28.9l-6.4-6.4" />
                                    <path id="Oval_32_15_" class="scercercwcwecw" d="M19.6,23.7c2.3,0,4.1-1.8,4.1-4.1s-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				S17.3,23.7,19.6,23.7z" />
                                    <path id="Shape_163_15_" class="scercercwcwecw" d="M26.8,51.4l-4.4,4.4" />
                                    <path id="Oval_33_15_" class="scercercwcwecw" d="M19.5,62.8c2.3,0,4.1-1.8,4.1-4.1s-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				S17.3,62.8,19.5,62.8z" />
                                    <path id="Shape_164_10_" class="scercercwcwecw" d="M48.4,38.1h6.2" />
                                    <path id="Oval_34_10_" class="scercercwcwecw" d="M58.7,42.2c2.3,0,4.1-1.8,4.1-4.1c0-2.3-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				C54.6,40.4,56.4,42.2,58.7,42.2z" />
                                    <path id="Shape_165_10_" class="scercercwcwecw" d="M29.9,38.1h-6.2" />
                                    <path id="Oval_35_13_" class="scercercwcwecw" d="M19.6,42.2c2.3,0,4.1-1.8,4.1-4.1c0-2.3-1.8-4.1-4.1-4.1c-2.3,0-4.1,1.8-4.1,4.1
				C15.4,40.4,17.3,42.2,19.6,42.2z" />
                                    <path id="Shape_166_34_" class="scercercwcwecw" d="M29.9,48.4c0-5.1,4.2-9.3,9.3-9.3c5.1,0,9.3,4.2,9.3,9.3H29.9z" />
                                    <path id="Oval_36_34_" class="scercercwcwecw" d="M39.1,35.6c3.1,0,5.7-2.5,5.7-5.7s-2.5-5.7-5.7-5.7c-3.1,0-5.7,2.5-5.7,5.7
				S36,35.6,39.1,35.6z" />
                                </g>
                            </g>
                        </g>
                    </svg>


                    <p>Full Platform</p>
                </a>

                <a href="/pricing3-free/" <?php if($wsMenuLvl2 == "2") {?> class="active" style="pointer-events: none;" <?php } else { } ?>>

                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 78.3 78.3" style="enable-background:new 0 0 78.3 78.3;" xml:space="preserve">
                        <style type="text/css">
                            .ertyuikujfhfg {
                                fill: none;
                                stroke: #008ECF;
                                stroke-width: 2;
                                stroke-linecap: round;
                                stroke-linejoin: round;
                                stroke-miterlimit: 10;
                            }

                        </style>
                        <g id="Business-Products__x2F__Business__x2F__user-network_33_">
                            <g id="Group_28_33_">
                                <g id="user-network_33_">
                                    <path id="Shape_166_33_" class="ertyuikujfhfg" d="M29.9,51.2c0-5.1,4.2-9.3,9.3-9.3c5.1,0,9.3,4.2,9.3,9.3H29.9z" />
                                    <path id="Oval_36_33_" class="ertyuikujfhfg" d="M39.1,38.4c3.1,0,5.7-2.5,5.7-5.7s-2.5-5.7-5.7-5.7c-3.1,0-5.7,2.5-5.7,5.7
				S36,38.4,39.1,38.4z" />
                                </g>
                            </g>
                        </g>
                    </svg>


                    <p>Free Products</p>
                </a>

            </div>
        </div>
    </div>


    <?php   
    } else {
        
    }
    ?>




</nav>
