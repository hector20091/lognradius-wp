<?php
/**
 * Footer full content
 */
?>

<footer id="footer" class="site-footer">
	<div class="container">

		<div class="site-info">
			<?php the_custom_logo(); ?>
		</div>

		<div class="footer-navigation">
			<?php
			echo wp_get_nav_menu_name( 'menu-products' );
			wp_nav_menu([
				'theme_location' => 'menu-products',
				'menu_id'        => 'products-menu',
			]);
			echo wp_get_nav_menu_name( 'menu-enterprises' );
			wp_nav_menu( [
				'theme_location' => 'menu-enterprises',
				'menu_id'        => 'enterprises-menu',
			]);
			echo wp_get_nav_menu_name( 'menu-developers' );
			wp_nav_menu([
				'theme_location' => 'menu-developers',
				'menu_id'        => 'developers-menu',
			]);
			echo wp_get_nav_menu_name( 'menu-resources' );
			wp_nav_menu([
				'theme_location' => 'menu-resources',
				'menu_id'        => 'resources-menu',
			]);
			echo wp_get_nav_menu_name( 'menu-about' );
			wp_nav_menu([
				'theme_location' => 'menu-about',
				'menu_id'        => 'about-menu',
			]);
			?>
		</div>
	</div>
</footer>
