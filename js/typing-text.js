jQuery(function ($) {

    var animationTime = 34400;
    var classy = "type-text";

    $(document).ready(function () {
        
        setInterval(function()
        {
            var element = $(`.${classy}`);

            element.removeClass(classy);
            
            // needed to update the styles and restart animation
            var temp = window.visualViewport.height;
            
            element.addClass(classy);

        }, animationTime);
    });
});
