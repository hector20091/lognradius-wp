<?php
/**
 * Template name: Integrations
 */

get_header();

// array of integration categories
$integration_categories = get_terms([
	'taxonomy' => 'integration_cat'
]);

get_template_part('components-sections/hero--integrations', null,
	[
		'title'      => get_field('hero_title'),
		'subtitle'   => get_field('hero_subtitle'),
		'categories' => $integration_categories
	]
);

foreach ( $integration_categories as $category ) {
	get_template_part('components-sections/cards--integrations', null,
		[
			'title'    => $category->name,
			'category' => $category->slug
		]
	);
}

get_footer();