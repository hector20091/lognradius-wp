<?php
/**
 * Template name: Case study
 * Template Post Type: resource
 */

get_header();

$sections = get_field('sections');

get_template_part('components-sections/hero--case-study', null,
	[
		'title'    => get_the_title(),
		'subtitle' => get_the_title(),
		'sections' => $sections
	]
);

foreach ( $sections as $section ) {
	get_template_part('components-sections/content--case-study', null,
		[
			'content' => get_field('hero_title'),
			'id'      => ''
		]
	);
}

get_footer();