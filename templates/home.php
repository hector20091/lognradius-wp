<?php
/**
 * Template name: Home
 */

get_header();

// Hero
get_template_part('components-sections/hero--homepage-01', null,
	[
		'title'    => get_field('hero_title'),
		'subtitle' => '',
		'Video'    => ''
	]
);

// Testimonials
get_template_part('components-sections/testimonials--4x4', null,
	[
		'title' => get_field('testimonials_title'),
	]
);

// Customers
get_template_part('components-sections/logos--grid', null,
	[
		'cards' => get_field('customers_list')
	]
);

// Global leader
get_template_part('components-sections/cards--images-graphic', null,
	[
		'title'    => get_field('leader_title'),
		'subtitle' => '',
		'cards'    => get_field('leader_list')
	]
);

// Integrations
get_template_part('components-sections/cards--integrations', null,
	[
		'title' => get_field('integrations_title'),
		'count' => 24                                       // count of posts
	]
);

// Resources
get_template_part('components-sections/cards--images-graphic', null,
	[
		'title'    => get_field('resources_title'),
		'subtitle' => '',
		'cards'    => get_field('resources_list')
	]
);

// CTA free sign up
get_template_part('components-sections/ctas--free-signup' );

get_footer();